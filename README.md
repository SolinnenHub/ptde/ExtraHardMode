ExtraHardMode PTDE
=============

Fork of [ExtraHardMode Bukkit Plugin](https://dev.bukkit.org/bukkit-plugins/fun-hard-mode/) made for Minecraft: Prepare To Die Edition ⓒ with the addition of new features:

* Stamina (consumed while running and using weapons).
* Armor weight (slows down movement depending on what armor you are wearing).
* Fine tuning of all parameters.
* Huge code refactoring.

[![ Minecraft: plugin for stamina and equipment weight. ](preview.jpg)](https://youtu.be/eAvGscD3wfE " Minecraft: plugin for stamina and equipment weight. ")

## Build instructions:

```bash
./build.sh
```
