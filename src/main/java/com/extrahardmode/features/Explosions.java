package com.extrahardmode.features;

import com.extrahardmode.ExtraHardMode;
import com.extrahardmode.config.ExplosionType;
import com.extrahardmode.config.RootConfig;
import com.extrahardmode.config.RootNode;
import com.extrahardmode.module.BlockModule;
import com.extrahardmode.module.UtilityModule;
import com.extrahardmode.service.ListenerModule;
import com.extrahardmode.task.CreateExplosionTask;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;


public class Explosions extends ListenerModule {
    private RootConfig CFG;

    private final String tag = "extrahardmode.explosion.fallingblock";

    public Explosions(ExtraHardMode plugin)
    {
        super(plugin);
    }

    @Override
    public void starting() {
        super.starting();
        CFG = plugin.getModuleForClass(RootConfig.class);
    }


    /**
     * Regular listener:
     * Bigger (custom) explosions
     */
    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGH) // High priority to allow plugins to clear the blocklist if they so choose
    public void regularExplosions(EntityExplodeEvent event) {
        if (!(event.getEntity() instanceof Ghast || event.getEntity() instanceof TNTPrimed))
            return;

        final Entity sourceEntity = event.getEntity();
        final World world = event.getLocation().getWorld();
        final Location location = sourceEntity.getLocation();

        final boolean flyOtherPlugins = CFG.getBoolean(RootNode.EXPLOSIONS_FYLING_BLOCKS_ENABLE_OTHER);
        final boolean customGhastExplosion = CFG.getBoolean(RootNode.EXPLOSIONS_GHASTS_ENABLE);
        final boolean customTntExplosion = CFG.getBoolean(RootNode.EXPLOSIONS_TNT_ENABLE);
        final boolean multipleExplosions = CFG.getBoolean(RootNode.BETTER_TNT);
        //cancel explosion if no worldDamage should be done
        final boolean tntWorldDamage = event.getLocation().getBlockY() > CFG.getInt(RootNode.EXPLOSIONS_Y)
                ? CFG.getBoolean(RootNode.EXPLOSIONS_TNT_ABOVE_WORLD_GRIEF)
                : CFG.getBoolean(RootNode.EXPLOSIONS_TNT_BELOW_WORLD_GRIEF);

        // TNT
        if (sourceEntity instanceof TNTPrimed) {
            //getYield value of 0.25 somewhat ensures this is a vanilla TNT explosion.
            if (customTntExplosion && event.blockList().size() > 0 && (flyOtherPlugins || event.getYield() == 0.25)) {
                if (!multipleExplosions) {
                    CreateExplosionTask explosionTask = new CreateExplosionTask(plugin, location, ExplosionType.TNT, sourceEntity);
                    plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, explosionTask, 1L);
                } else { //multiple explosions will also handle the custom size
                    multipleExplosions(location, sourceEntity, ExplosionType.TNT);
                }
                if (!tntWorldDamage) {
                    assert world != null;
                    if (CFG.isEnabledIn(world.getName())) {
                        event.setCancelled(true);
                    }
                }
            }
        }

        // GHASTS
        else if (sourceEntity instanceof Fireball) {
            if (customGhastExplosion) {
                Fireball fireball = (Fireball) sourceEntity;
                if (fireball.getShooter() instanceof Ghast) {
                    event.setCancelled(true);
                    // same as vanilla TNT, plus fire
                    new CreateExplosionTask(plugin, sourceEntity.getLocation(), ExplosionType.GHAST_FIREBALL, sourceEntity).run();
                }
            }
        }
    }


    /**
     * This gets called late so we know the explosion has been allowed
     */
    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST) //so it probably got cancelled already
    public void onLateExplosion(EntityExplodeEvent event) {
        final Location location = event.getLocation();
        final Collection<Block> blocks = event.blockList();

        final boolean flyingBlocks = CFG.getBoolean(RootNode.EXPLOSIONS_FYLING_BLOCKS_ENABLE);

        final int flyPercentage = CFG.getInt(RootNode.EXPLOSIONS_FLYING_BLOCKS_PERCENTAGE);
        final double upVel = CFG.getDouble(RootNode.EXPLOSIONS_FLYING_BLOCKS_UP_VEL);
        final double spreadVel = CFG.getDouble(RootNode.EXPLOSIONS_FLYING_BLOCKS_SPREAD_VEL);

        // PHYSICS
        if (flyingBlocks) {
            applyExplosionPhysics(blocks, location, flyPercentage, upVel, spreadVel);
        }
    }


    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true) //so we are last and if a block protection plugin cancelled the event we know it
    public void handleLandedBlocksFromPhysics(EntityChangeBlockEvent event) {
        final int distance = (int) Math.pow(CFG.getInt(RootNode.EXPLOSIONS_FLYING_BLOCKS_AUTOREMOVE_RADIUS), 2);
        if (event.getEntity() instanceof FallingBlock) {
            Block block = event.getBlock();
            FallingBlock fallBaby = (FallingBlock) event.getEntity();
            if (fallBaby.hasMetadata(tag)) {
                Object obj = fallBaby.getMetadata(tag).size() > 0 ? fallBaby.getMetadata(tag).get(0).value() : null;
                if (obj instanceof Location) {
                    Location loc = (Location) obj;
                    //Compare the distance to the original explosion, dont place block if the block landed far away (dont make landscape ugly)
                    if (event.getBlock().getLocation().distanceSquared(loc) > distance) {
                        event.setCancelled(true);
                        fallBaby.remove();
                    } else { //If close place the block as if the player broke it first: stone -> cobble, gras -> dirt etc.
                        Material type = BlockModule.getDroppedMaterial(fallBaby.getBlockData().getMaterial());
                        if (type.isBlock() && type == fallBaby.getBlockData().getMaterial()) { //preserve blockdata. See issue #69 //Alternatively could block#setType in second condition
                            return;
                        } else if (type.isBlock()) {
                            block.setType(type);
                        } else { //if block doesn't drop something that can be placed again... thin glass, redstone ore
                            block.setType(Material.AIR);
                        }
                        event.setCancelled(true);
                    }
                }
            }
        }
    }

    /**
     * Creates 4 additional explosions around a location to make the shape of the crater random and more natural
     *
     * @param location      center location to create explosions
     * @param sourceEntity  entity that caused the explosion
     * @param explosionType type determines the size of the explosion
     */
    @SuppressWarnings("IntegerDivisionInFloatingPointContext")
    public void multipleExplosions(Location location, Entity sourceEntity, ExplosionType explosionType) {
        // create more explosions nearby
        long serverTime = Objects.requireNonNull(location.getWorld()).getFullTime();
        int random1 = (int) (serverTime + location.getBlockZ()) % 8;
        int random2 = (int) (serverTime + location.getBlockX()) % 8;

        Location[] locations = new Location[] {
                location.add(random1, 1, random2),
                location.add(-random2, 0, random1 / 2),
                location.add(-random1 / 2, -1, -random2),
                location.add(random1 / 2, 0, -random2 / 2)
        };

        final int explosionsNum = locations.length;

        for (int i = 0; i < explosionsNum; i++) {
            CreateExplosionTask task = new CreateExplosionTask(plugin, locations[i], explosionType, sourceEntity);
            plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, task, 3L * (i + 1));
        }
    }


    /**
     * Make blocks fly
     *
     * @param blocks        list of blocks
     * @param center        center from which to spread blocks out
     * @param flyPercentage percentage of blocks affected
     * @param upVel         how fast to propel upwards
     * @param spreadVel     how fast to propel on horizontal axis
     */
    public void applyExplosionPhysics(Collection<Block> blocks, final Location center, final int flyPercentage, final double upVel, final double spreadVel) {
        final List<FallingBlock> fallingBlockList = new ArrayList<>();
        for (Block block : blocks) {
            if (block.getType().isSolid()) {
                //Only a few of the blocks fly as an effect
                if (plugin.random(flyPercentage)) {
                    FallingBlock fall = Objects.requireNonNull(block.getLocation().getWorld()).spawnFallingBlock(block.getLocation(), block.getBlockData());
                    fall.setMetadata(tag, new FixedMetadataValue(plugin, block.getLocation())); //decide on the distance if block should be placed
                    //fall.setMetadata("drops", new FixedMetadataValue(plugin, block.getDrops()));
                    fall.setDropItem(CFG.getBoolean(RootNode.MORE_FALLING_BLOCKS_DROP_ITEM));
                    UtilityModule.moveUp(fall, upVel);
                    //block.setType(Material.AIR);
                    fallingBlockList.add(fall);
                }
            }
        }

        plugin.getServer().getScheduler().runTaskLater(plugin, () -> {
            for (FallingBlock fall : fallingBlockList) {
                UtilityModule.moveAway(fall, center, spreadVel);
            }
        }, 2L);
    }
}