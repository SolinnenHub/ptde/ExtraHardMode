package com.extrahardmode.features;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketContainer;
import com.extrahardmode.ExtraHardMode;
import com.extrahardmode.config.RootConfig;
import com.extrahardmode.config.RootNode;
import com.extrahardmode.enchantments.CustomEnchants;
import com.extrahardmode.module.PlayerModule;
import com.extrahardmode.module.ScoreboardStatusManager;
import com.extrahardmode.service.ListenerModule;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.block.Block;
import org.bukkit.entity.AbstractArrow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.lang.reflect.InvocationTargetException;
import java.util.*;

import static com.extrahardmode.ExtraHardMode.staminaKey;
import static java.lang.Math.*;
import static org.bukkit.util.NumberConversions.square;

public class Movement extends ListenerModule {

    private final ScoreboardStatusManager scoreboardStatusManager;
    private PlayerModule playerModule;

    private static final int BASE_ATTACK_SPEED = 4;

    private static final float STAMINA_RESTORE_PER_SECOND = 45f;
    private static final float STAMINA_RESTORE_PER_TICK = STAMINA_RESTORE_PER_SECOND / 20f;
    private static final float MAXIMUM_STAMINA = 110f;
    private static final float MIN_STAMINA_FOR_ACTION = 3f;
    private static final float DAMAGE_TO_STAMINA_MULTIPLIER = 10f;
    private static final int NO_STAMINA_REGEN_PERIOD_MILLS = round(1.533f * 1000); // seconds * 1000 mills
    private static final int NO_STAMINA_REGEN_PERIOD_TICKS = round(20 * NO_STAMINA_REGEN_PERIOD_MILLS / 1000f);
    private static final int NO_DOGE_MILLS = 800;
    private static final double DOGE_POWER_MULTIPLIER = -0.64;

    private static final float SPRINT_C = 0.60f;
    private static final float DOGE_C = 30f;
    private static final float BOW_SHOT_C = 35f;
    private static final float CROSSBOW_SHOT_C = 15f;
    private static final float TRIDENT_SHOT_C = 80f;
    private static final float SNOWBALL_SHOT_C = 7f;

    private static class StaminaState {

        private float value;
        public long noRegenUntil = 0;
        public boolean isBlocked = false;

        public StaminaState(float value) {
            this.value = value;
        }

        public float get() {
            return value;
        }

        private float fit(float value) {
            if (value > MAXIMUM_STAMINA) {
                return MAXIMUM_STAMINA;
            }
            return Math.max(0, value);
        }

        public void add(float delta) {
            this.value = fit(value + delta);
        }

        public boolean tryToRestore(float delta) {
            if (System.currentTimeMillis() > this.noRegenUntil) {
                this.value = fit(value + delta);
                return true;
            }
            return false;
        }

        public void subtract(float delta, int no_regen_ticks) {
            this.value = fit(value - delta);
            this.noRegenUntil = max(this.noRegenUntil, System.currentTimeMillis() + 50L*no_regen_ticks);
        }
    }

    private float baseSpeed;

    private final HashMap<Material, Float> weaponStaminaConsumption = new HashMap<>();
    private final HashMap<UUID, Float> lastWeightPoints = new HashMap<>(100);
    private final HashMap<UUID, Long> combatTags = new HashMap<>(30);
    private final HashMap<UUID, Long> playersBlockHits = new HashMap<>(30);
    private final HashMap<UUID, Integer> lockedHunger = new HashMap<>(30);
    private final HashMap<UUID, StaminaState> playersStamina = new HashMap<>(100);
    private final HashMap<UUID, StaminaRegenModificator> playersStaminaRegenModificators = new HashMap<>(100);

    private final HashMap<UUID, Long> playersSneakingTogglesTimestamps = new HashMap<>(100);
    private final HashMap<UUID, Long> playersSneakingPatternsTimestamps = new HashMap<>(100);
    private final HashMap<UUID, Long> playersDogeTimestamps = new HashMap<>(100);

    private static final HashSet<Material> tools = new HashSet<>(Arrays.asList(
            Material.STONE_PICKAXE,
            Material.IRON_PICKAXE,
            Material.DIAMOND_PICKAXE,
            Material.NETHERITE_PICKAXE,
            Material.STONE_SHOVEL,
            Material.IRON_SHOVEL,
            Material.DIAMOND_SHOVEL,
            Material.NETHERITE_SHOVEL,
            Material.WOODEN_PICKAXE,
            Material.WOODEN_SHOVEL,
            Material.GOLDEN_PICKAXE,
            Material.GOLDEN_SHOVEL,
            Material.SHEARS
    ));

    private static final HashSet<Material> swords = new HashSet<>(Arrays.asList(
            Material.IRON_SWORD,
            Material.DIAMOND_SWORD,
            Material.NETHERITE_SWORD,
            Material.STONE_SWORD,
            Material.GOLDEN_SWORD,
            Material.WOODEN_SWORD
    ));

    public Movement(ExtraHardMode plugin) {
        super(plugin);

        scoreboardStatusManager = plugin.getModuleForClass(ScoreboardStatusManager.class);

        weaponStaminaConsumption.put(Material.AIR, 20f);

        weaponStaminaConsumption.put(Material.WOODEN_SWORD, 15f);
        weaponStaminaConsumption.put(Material.STONE_SWORD, 20f);
        weaponStaminaConsumption.put(Material.IRON_SWORD, 25f);
        weaponStaminaConsumption.put(Material.GOLDEN_SWORD, 30f);
        weaponStaminaConsumption.put(Material.DIAMOND_SWORD, 30f);
        weaponStaminaConsumption.put(Material.NETHERITE_SWORD, 35f);

        weaponStaminaConsumption.put(Material.WOODEN_AXE, 20f);
        weaponStaminaConsumption.put(Material.STONE_AXE, 25f);
        weaponStaminaConsumption.put(Material.IRON_AXE, 30f);
        weaponStaminaConsumption.put(Material.GOLDEN_AXE, 35f);
        weaponStaminaConsumption.put(Material.DIAMOND_AXE, 35f);
        weaponStaminaConsumption.put(Material.NETHERITE_AXE, 40f);
    }

    private static class StaminaRegenModificator {

        public int NO_STAMINA_REGEN_PERIOD_TICKS;
        public float STAMINA_RESTORE_PER_TICK;
        public float DOGE_C;
        public double DOGE_POWER_MULTIPLIER;

        public StaminaRegenModificator(Player player, float weightPointsNorm) {
            this.NO_STAMINA_REGEN_PERIOD_TICKS = Movement.NO_STAMINA_REGEN_PERIOD_TICKS;
            this.STAMINA_RESTORE_PER_TICK = Movement.STAMINA_RESTORE_PER_TICK;
            this.DOGE_C = Movement.DOGE_C;
            this.DOGE_POWER_MULTIPLIER = Movement.DOGE_POWER_MULTIPLIER;

            PlayerInventory inv = player.getInventory();
            ItemMeta meta;

            meta = inv.getItemInMainHand().getItemMeta();
            if (meta != null) {
                if (meta.hasEnchant(CustomEnchants.STAMINA_RESTORATION)) {
                    staminaRestoration(meta);
                }
            }
            meta = inv.getItemInOffHand().getItemMeta();
            if (meta != null) {
                if (meta.hasEnchant(CustomEnchants.STAMINA_RESTORATION)) {
                    staminaRestoration(meta);
                }
            }

            if (weightPointsNorm > 0.70) { // Armor influence
                this.STAMINA_RESTORE_PER_TICK *= 0.80f;
                this.NO_STAMINA_REGEN_PERIOD_TICKS = (int) (this.NO_STAMINA_REGEN_PERIOD_TICKS * 1.18f);
                this.DOGE_C *= 1.20;
            } else if (weightPointsNorm <= 0.30) {
                this.DOGE_POWER_MULTIPLIER *= 1.08;
            }
        }

        private void staminaRestoration(ItemMeta meta) {
            int level = meta.getEnchantLevel(CustomEnchants.STAMINA_RESTORATION);
            this.STAMINA_RESTORE_PER_TICK += (3 + level) / 20f;
            this.NO_STAMINA_REGEN_PERIOD_TICKS = (int) (this.NO_STAMINA_REGEN_PERIOD_TICKS * 0.85);
        }
    }

    @Override
    public void starting() {
        super.starting();
        RootConfig CFG = plugin.getModuleForClass(RootConfig.class);
        playerModule = plugin.getModuleForClass(PlayerModule.class);

        // Stamina & Movement
        plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
            for (Player player : plugin.getServer().getOnlinePlayers()) {
                if (playerModule.playerBypasses(player)) {
                    continue;
                }

                UUID uuid = player.getUniqueId();

                if (!playersStamina.containsKey(uuid)) {
                    playersStamina.put(uuid, new StaminaState(MAXIMUM_STAMINA));
                }

                StaminaState stamina = playersStamina.get(uuid);
                final float weightPointsNorm = PlayerModule.getWeightPointsNorm(player, false);
                StaminaRegenModificator modificator = playersStaminaRegenModificators.get(uuid);
                if (modificator == null) {
                    modificator = forceUpdateStaminaRegenModificator(player, weightPointsNorm);
                }

                boolean idle = true;
                if (stamina.get() <= MIN_STAMINA_FOR_ACTION) {
                    onZeroStamina(stamina, player);
                } else {
                    if (player.isBlocking()) {
                        idle = false;
                        if (stamina.tryToRestore(modificator.STAMINA_RESTORE_PER_TICK / 3f)) {
                            cancelStaminaEffects(stamina, player);
                        }
                    } else if (player.isHandRaised()) {
                        idle = false;
                    } else if (player.isSprinting()) {
                        stamina.subtract(SPRINT_C, modificator.NO_STAMINA_REGEN_PERIOD_TICKS);
                        idle = false;
                    }
                }

                // No sprinting if too heavy
                if (player.isSprinting()) {
                    if (player.isSwimming()) {
                        if (weightPointsNorm > 0.70) {
                            player.setSprinting(false);
                        }
                    } else {
                        if (weightPointsNorm > 1.00) {
                            player.setSprinting(false);
                        } else if (weightPointsNorm > 0.90) {
                            Vector dir = player.getVelocity();
                            double euclidianMetric = sqrt(square(dir.getX()) + square(dir.getZ()));
                            if (euclidianMetric > 0.20) {
                                player.setSprinting(false);
                            }
                        }
                    }
                }

                if (idle) {
                    if (stamina.tryToRestore(modificator.STAMINA_RESTORE_PER_TICK)) {
                        cancelStaminaEffects(stamina, player);
                    }
                }

                displayStamina(player, stamina);
            }
        }, 6, 1);

        baseSpeed = (float) CFG.getDouble(RootNode.ARMOR_SLOWDOWN_BASESPEED);
        final int armorSlowdownPercent = CFG.getInt(RootNode.ARMOR_SLOWDOWN_PERCENT);

        // Weight display
        plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
            for (Player player : plugin.getServer().getOnlinePlayers()) {
                UUID uuid = player.getUniqueId();
                final float weightPointsNorm = PlayerModule.getWeightPointsNorm(player, true);
                forceUpdateStaminaRegenModificator(player, weightPointsNorm);

                if (lastWeightPoints.getOrDefault(uuid, -1f) != weightPointsNorm) {
                    lastWeightPoints.put(uuid, weightPointsNorm);
                    String text = String.format(Locale.US, "Вес: %.1f (%.0f%%)", weightPointsNorm*PlayerModule.norm, weightPointsNorm*100);
                    scoreboardStatusManager.showPopup(player, "Показатели", text);
                }
            }
        }, 20, 10);


        // Weight slowdown
        plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
            for (Player player : plugin.getServer().getOnlinePlayers()) {
                if (playerModule.playerBypasses(player)) {
                    if (player.getWalkSpeed() != baseSpeed) {
                        player.setWalkSpeed(baseSpeed);
                    }
                    continue;
                }

                final float weightPointsNorm = PlayerModule.getWeightPointsNorm(player, false);
                if (weightPointsNorm != 0) {
                    float value = max(baseSpeed * (1 - weightPointsNorm * (armorSlowdownPercent / 100F)), 0);
                    if (player.getWalkSpeed() != value) {
                        player.setWalkSpeed(value);
                    }
                } else if (player.getWalkSpeed() != baseSpeed) {
                    player.setWalkSpeed(baseSpeed);
                }
            }
        }, 20, 50);
    }

    private StaminaRegenModificator forceUpdateStaminaRegenModificator(Player player, float weightPointsNorm) {
        StaminaRegenModificator modificator = new StaminaRegenModificator(player, weightPointsNorm);
        playersStaminaRegenModificators.put(player.getUniqueId(), modificator);
        return modificator;
    }

    /**
     *  DOGES
     */
    @EventHandler(priority = EventPriority.NORMAL)
    private void onPlayerToggleSneak(PlayerToggleSneakEvent event) {
        Player player = event.getPlayer();
        if (player.isOnGround() && !player.isDead()) {
            final Location loc = player.getLocation();
            final Block underPlayer = player.getWorld().getBlockAt(loc.getBlockX(), loc.getBlockY() - 1, loc.getBlockZ());
            if (underPlayer.getType().isSolid()) {
                final Block atPlayer = player.getWorld().getBlockAt(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
                if (atPlayer.getType() != Material.LAVA && atPlayer.getType() != Material.WATER) {
                    // мы на твердой поверхности

                    final float weightPointsNorm = PlayerModule.getWeightPointsNorm(player, false);
                    UUID uuid = player.getUniqueId();
                    final StaminaState stamina = playersStamina.get(uuid);

                    if (stamina.get() >= MIN_STAMINA_FOR_ACTION && (weightPointsNorm <= 0.90)) {
                        final long currentTime = System.currentTimeMillis();
                        if (currentTime - playersSneakingTogglesTimestamps.getOrDefault(uuid, 0L) < 220) {
                            if (currentTime - playersSneakingPatternsTimestamps.getOrDefault(uuid, 0L) < 260) {
                                // распознан паттерн отскока
                                if (currentTime - playersDogeTimestamps.getOrDefault(uuid, 0L) > NO_DOGE_MILLS) {
                                    // отскок совершается не слишком часто
                                    Vector vector = player.getEyeLocation().getDirection();
                                    double norm = sqrt(square(vector.getX()) + square(vector.getZ()));
                                    StaminaRegenModificator modificator = playersStaminaRegenModificators.get(uuid);
                                    if (modificator == null) {
                                        modificator = forceUpdateStaminaRegenModificator(player, weightPointsNorm);
                                    }
                                    player.setVelocity(new Vector(
                                            modificator.DOGE_POWER_MULTIPLIER * vector.getX() / norm, 0.2,
                                            modificator.DOGE_POWER_MULTIPLIER * vector.getZ() / norm));
                                    if (!playerModule.playerBypasses(player)) {
                                        stamina.subtract(modificator.DOGE_C, modificator.NO_STAMINA_REGEN_PERIOD_TICKS);
                                    }
                                    playersDogeTimestamps.put(uuid, currentTime);
                                }
                            }
                            playersSneakingPatternsTimestamps.put(uuid, currentTime);
                        }
                        playersSneakingTogglesTimestamps.put(uuid, currentTime);
                    }
                }
            }
        }
    }

    private void onZeroStamina(StaminaState stamina, Player player) {
        if (!stamina.isBlocked) {
            stamina.isBlocked = true;

            int ticks = round(20 * (stamina.noRegenUntil - System.currentTimeMillis()) / 1000f);
            if (ticks > 0) {
                player.setCooldown(Material.BOW, ticks);
                player.setCooldown(Material.CROSSBOW, ticks);
                player.setCooldown(Material.SHIELD, ticks);
                player.setCooldown(Material.TRIDENT, ticks);
                player.setCooldown(Material.SNOWBALL, ticks);

                player.addPotionEffect(new PotionEffect(
                        PotionEffectType.SLOW_DIGGING, ticks, 9, false, false, false));

                AttributeInstance attackSpeed = player.getAttribute(Attribute.GENERIC_ATTACK_SPEED);
                if (attackSpeed != null) {
                    attackSpeed.setBaseValue(0);
                }
            }
        }

        player.setSprinting(false);

        if (player.getFoodLevel() > 6) {
            UUID uuid = player.getUniqueId();
            if (!lockedHunger.containsKey(uuid)) {
                lockedHunger.put(uuid, player.getFoodLevel());
                player.setFoodLevel(6);
            }
        }
    }

    private void cancelStaminaEffects(StaminaState stamina, Player player) {
        if (stamina.isBlocked) {
            stamina.isBlocked = false;

            player.setCooldown(Material.BOW, 0);
            player.setCooldown(Material.CROSSBOW, 0);
            player.setCooldown(Material.SHIELD, 0);
            player.setCooldown(Material.TRIDENT, 0);
            player.setCooldown(Material.SNOWBALL, 0);

            player.removePotionEffect(PotionEffectType.SLOW_DIGGING);

            AttributeInstance attackSpeed = player.getAttribute(Attribute.GENERIC_ATTACK_SPEED);
            if (attackSpeed != null) {
                attackSpeed.setBaseValue(BASE_ATTACK_SPEED);
            }

            UUID uuid = player.getUniqueId();
            if (lockedHunger.containsKey(uuid)) {
                player.setFoodLevel(lockedHunger.get(uuid));
                lockedHunger.remove(uuid);
            }
        }
    }

    private void displayStamina(Player player, StaminaState state) {
        PacketContainer packet = new PacketContainer(PacketType.Play.Server.EXPERIENCE);
        packet.getFloat().write(0, state.get() / MAXIMUM_STAMINA);
        packet.getIntegers()
                .write(0, player.getTotalExperience())
                .write(1, player.getLevel());
        packet.setMeta(staminaKey, true);
        try {
            ExtraHardMode.protocolManager.sendServerPacket(player, packet);
        } catch (InvocationTargetException e) {
            throw new RuntimeException("Cannot send packet " + packet, e);
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    private void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        playersStamina.put(player.getUniqueId(), new StaminaState(MAXIMUM_STAMINA));
        AttributeInstance attackSpeed = player.getAttribute(Attribute.GENERIC_ATTACK_SPEED);
        if (attackSpeed != null) {
            attackSpeed.setBaseValue(BASE_ATTACK_SPEED);
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    private void onPlayerInteractEvent(PlayerInteractEvent event) {
        if (event.getHand() == null) {
            return;
        }
        if (event.getHand().equals(EquipmentSlot.HAND)) {
            Player player = event.getPlayer();
            if (playerModule.playerBypasses(player)) {
                return;
            }

            UUID uuid = player.getUniqueId();
            ItemStack itemInHand = event.getItem();

            if (event.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
                if (itemInHand != null && tools.contains(itemInHand.getType())) {
                    return;
                } // в руках не инструмент

                long currentTime = System.currentTimeMillis();

                if (playersBlockHits.containsKey(uuid)) {
                    if (currentTime - playersBlockHits.get(uuid) <= 500) {
                        playersBlockHits.put(uuid, currentTime);
                        return;
                    }
                }
                playersBlockHits.put(uuid, currentTime);

                if (itemInHand == null || !swords.contains(itemInHand.getType())) {
                    if (combatTags.containsKey(uuid)) {
                        if (currentTime - combatTags.get(uuid) > 6000) {
                            combatTags.remove(uuid);
                            return;
                        }
                    } else {
                        return;
                    }
                }
            }

            if (event.getAction().equals(Action.LEFT_CLICK_AIR) || event.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
                float weightPointsNorm = PlayerModule.getWeightPointsNorm(player, false);
                StaminaRegenModificator modificator = new StaminaRegenModificator(player, weightPointsNorm);
                StaminaState stamina = playersStamina.get(uuid);

                if (itemInHand == null) {
                    stamina.subtract(weaponStaminaConsumption.get(Material.AIR), modificator.NO_STAMINA_REGEN_PERIOD_TICKS);
                } else if (stamina.get() > 0) {
                    if (weaponStaminaConsumption.containsKey(itemInHand.getType())) {
                        stamina.subtract(weaponStaminaConsumption.get(itemInHand.getType()), modificator.NO_STAMINA_REGEN_PERIOD_TICKS);
                    } else {
                        stamina.subtract(weaponStaminaConsumption.get(Material.AIR), modificator.NO_STAMINA_REGEN_PERIOD_TICKS);
                    }
                } else {
                    onZeroStamina(stamina, player);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    private void onPlayerDamage(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player) {
            Player damager = (Player) event.getDamager();
            combatTags.put(damager.getUniqueId(), System.currentTimeMillis());
            StaminaState stamina = playersStamina.get(damager.getUniqueId());
            if (stamina.get() <= MIN_STAMINA_FOR_ACTION) {
                event.setCancelled(true);
            }
        }

        if (event.getEntity() instanceof Player) {
            Player victim = (Player) event.getEntity();
            combatTags.put(victim.getUniqueId(), System.currentTimeMillis());
            float weightPointsNorm = PlayerModule.getWeightPointsNorm(victim, false);
            StaminaRegenModificator modificator = new StaminaRegenModificator(victim, weightPointsNorm);
            if (victim.isBlocking()) {
                StaminaState stamina = playersStamina.get(victim.getUniqueId());
                stamina.subtract((float) (event.getDamage() * DAMAGE_TO_STAMINA_MULTIPLIER), modificator.NO_STAMINA_REGEN_PERIOD_TICKS);
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    private void onProjectileLaunchEvent(ProjectileLaunchEvent event) {
        if (event.getEntity().getShooter() instanceof Player) {
            Projectile projectile = event.getEntity();
            Player player = (Player) projectile.getShooter();
            if (player.getGameMode() == GameMode.CREATIVE) {
                return;
            }

            StaminaState stamina = playersStamina.get(player.getUniqueId());
            float weightPointsNorm = PlayerModule.getWeightPointsNorm(player, false);
            StaminaRegenModificator modificator = new StaminaRegenModificator(player, weightPointsNorm);

            if (stamina.get() > 0) {
                if (projectile.getType() == EntityType.SNOWBALL) {
                    stamina.subtract(SNOWBALL_SHOT_C, modificator.NO_STAMINA_REGEN_PERIOD_TICKS);
                } else if (projectile.getType() == EntityType.ARROW || projectile.getType() == EntityType.SPECTRAL_ARROW) {
                    AbstractArrow arrow = (AbstractArrow) projectile;
                    if (arrow.isShotFromCrossbow()) {
                        stamina.subtract(CROSSBOW_SHOT_C, modificator.NO_STAMINA_REGEN_PERIOD_TICKS);
                    } else {
                        stamina.subtract(BOW_SHOT_C, modificator.NO_STAMINA_REGEN_PERIOD_TICKS);
                    }
                } else if (projectile.getType() == EntityType.TRIDENT) {
                    stamina.subtract(TRIDENT_SHOT_C, modificator.NO_STAMINA_REGEN_PERIOD_TICKS);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    private void onPlayerQuitEvent(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        if (player.getWalkSpeed() != baseSpeed) {
            player.setWalkSpeed(baseSpeed);
        }
        UUID uuid = player.getUniqueId();
        playersStamina.remove(uuid);
        playersStaminaRegenModificators.remove(uuid);
        playersBlockHits.remove(uuid);
        playersSneakingTogglesTimestamps.remove(uuid);
        playersSneakingPatternsTimestamps.remove(uuid);
        playersDogeTimestamps.remove(uuid);
        lockedHunger.remove(uuid);
        combatTags.remove(uuid);
    }
}
