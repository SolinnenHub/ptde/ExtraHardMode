package com.extrahardmode.features;

import com.extrahardmode.ExtraHardMode;
import com.extrahardmode.config.RootConfig;
import com.extrahardmode.config.RootNode;
import com.extrahardmode.module.PlayerModule;
import com.extrahardmode.service.ListenerModule;
import com.extrahardmode.service.config.customtypes.PotionEffectHolder;
import com.extrahardmode.task.SetPlayerHealthAndFoodTask;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.block.Block;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.potion.PotionEffectType;

public class Players extends ListenerModule {

    private PlayerModule playerModule;

    private int extinguishingFireIgnitesPlayersPercentage;
    private int playerRespawnHealthPercentage;
    private int playerRespawnFoodLevel;
    private boolean extinguishingFireIgnitesPlayers;
    private PotionEffectHolder enhancedDmgExplosionPotionEffect;
    private PotionEffectHolder enhancedDmgFallPotionEffect;
    private PotionEffectHolder enhancedDmgDrowningPotionEffect;

    public Players(ExtraHardMode plugin) {
        super(plugin);
    }

    @Override
    public void starting() {
        super.starting();
        RootConfig CFG = plugin.getModuleForClass(RootConfig.class);
        playerModule = plugin.getModuleForClass(PlayerModule.class);

        extinguishingFireIgnitesPlayersPercentage = CFG.getInt(RootNode.EXTINGUISHING_FIRE_IGNITES_PLAYERS_PERCENTAGE);

        playerRespawnHealthPercentage = CFG.getInt(RootNode.PLAYER_RESPAWN_HEALTH_PERCENTAGE);
        playerRespawnFoodLevel = CFG.getInt(RootNode.PLAYER_RESPAWN_FOOD_LEVEL);
        extinguishingFireIgnitesPlayers = CFG.getBoolean(RootNode.EXTINGUISHING_FIRE_IGNITES_PLAYERS);

        enhancedDmgExplosionPotionEffect = new PotionEffectHolder(PotionEffectType.CONFUSION, 20*8, 2);
        enhancedDmgFallPotionEffect = new PotionEffectHolder(PotionEffectType.SLOW, 20*3, 0);
        enhancedDmgDrowningPotionEffect = new PotionEffectHolder(PotionEffectType.BLINDNESS, 20, 1);
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerRespawn(PlayerRespawnEvent respawnEvent) {
        Player player = respawnEvent.getPlayer();

        final boolean playerBypasses = playerModule.playerBypasses(player);

        final int respawnHealthPercentage = playerBypasses ? 100 : playerRespawnHealthPercentage;
        final int respawnFood = playerBypasses ? 20 : playerRespawnFoodLevel;

        if (respawnFood < 20 && respawnHealthPercentage > 0 && respawnHealthPercentage < 100) {
            AttributeInstance health = player.getAttribute(Attribute.GENERIC_MAX_HEALTH);
            if (health != null) {
                SetPlayerHealthAndFoodTask task = new SetPlayerHealthAndFoodTask(player, (int) health.getValue() * respawnHealthPercentage / 100, respawnFood);
                plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, task, 10L); // half-second delay
            }
        }
    }

    /** FEATURE
     * Environmental effects when player is damaged
     */
    @EventHandler(priority = EventPriority.HIGHEST) //so we know if the event got cancelled
    public void onEntityDamage(EntityDamageEvent event) {
        Entity entity = event.getEntity();

        if (entity instanceof Player) {
            Player player = (Player) entity;
            if (!playerModule.playerBypasses(player)) {
                EntityDamageEvent.DamageCause cause = event.getCause();
                switch (cause) {
                    case BLOCK_EXPLOSION:
                    case ENTITY_EXPLOSION:
                        if (event.getDamage() > 2) {
                            enhancedDmgExplosionPotionEffect.applyEffect((LivingEntity) event.getEntity(), false);
                        }
                        break;
                    case FALL:
                        enhancedDmgFallPotionEffect.applyEffect((LivingEntity) event.getEntity(), false);
                        event.setDamage(event.getDamage() * 1.3);
                        break;
                    case LAVA:
                        event.setDamage(event.getDamage() * 2);
                        break;
                    case DROWNING:
                        enhancedDmgDrowningPotionEffect.applyEffect((LivingEntity) event.getEntity(), false);
                        break;
                }
            }
        }
    }


    /** FEATURE
     * putting out fire up close catches the player on fire
     */
    @EventHandler(priority = EventPriority.LOWEST)
    void onPlayerInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        Block block = event.getClickedBlock();
        Action action = event.getAction();

        final boolean playerBypasses = playerModule.playerBypasses(player);

        if (extinguishingFireIgnitesPlayers && !playerBypasses && block != null && block.getType() != Material.AIR &&
                (action.equals(Action.LEFT_CLICK_BLOCK) || action.equals(Action.LEFT_CLICK_AIR))) {
            if (block.getRelative(event.getBlockFace()).getType() == Material.FIRE) {
                if (plugin.random(extinguishingFireIgnitesPlayersPercentage)) {
                    event.setCancelled(true);
                    player.setFireTicks(70);
                }
            }
        }
    }
}
