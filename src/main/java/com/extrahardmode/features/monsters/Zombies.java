package com.extrahardmode.features.monsters;

import com.extrahardmode.ExtraHardMode;
import com.extrahardmode.module.PlayerModule;
import com.extrahardmode.service.ListenerModule;
import com.extrahardmode.service.config.customtypes.PotionEffectHolder;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.potion.PotionEffectType;

/** Zombies can resurrect themselves, make players slow when hit */
public class Zombies extends ListenerModule {

    private PlayerModule playerModule;
    private final PotionEffectHolder effect = new PotionEffectHolder(PotionEffectType.SLOW, 5 * 20, 1);

    public Zombies(ExtraHardMode plugin) {
        super(plugin);
    }

    @Override
    public void starting() {
        super.starting();
        playerModule = plugin.getModuleForClass(PlayerModule.class);
    }

    /**
     * When an Entity is damaged
     * <p/>
     * When a player is damaged by a zombie make him slow
     */
    @EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
    public void onEntityDamage(EntityDamageEvent event) {
        Entity entity = event.getEntity();

        if (entity.getType().equals(EntityType.PLAYER)) {

            // is this an entity damaged by entity event?
            EntityDamageByEntityEvent damageByEntityEvent = null;
            if (event instanceof EntityDamageByEntityEvent) {
                damageByEntityEvent = (EntityDamageByEntityEvent) event;
            }

            Player player = (Player) entity;

            // FEATURE: zombies can apply a debilitating effect
            if (!playerModule.playerBypasses(player)) {
                if (damageByEntityEvent != null && damageByEntityEvent.getDamager().getType().equals(EntityType.ZOMBIE)) {
                    effect.applyEffect(player, false);
                }
            }
        }
    }
}
