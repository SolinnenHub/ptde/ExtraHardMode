package com.extrahardmode.features.monsters;

import com.extrahardmode.ExtraHardMode;
import com.extrahardmode.config.ExplosionType;
import com.extrahardmode.config.RootConfig;
import com.extrahardmode.config.RootNode;
import com.extrahardmode.module.EntityHelper;
import com.extrahardmode.service.ListenerModule;
import com.extrahardmode.task.CreateExplosionTask;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

/**
 * Changes to Blazes including:
 * spawn in the Nether everywhere , multiply on death , more loot , magmacubes explode on death and turn into Blazes
 * spawn at lavalevel in the OverWorld , explode on death in the Overworld , no blazerods in the OverWorld
 */
public class Blazes extends ListenerModule {

    int bonusNetherBlazeSpawnPercent;
    int nearBedrockSpawnPercent;

    public Blazes(ExtraHardMode plugin) {
        super(plugin);
    }

    @Override
    public void starting() {
        super.starting();
        RootConfig CFG = plugin.getModuleForClass(RootConfig.class);

        bonusNetherBlazeSpawnPercent = CFG.getInt(RootNode.BONUS_NETHER_BLAZE_SPAWN_PERCENT);
        nearBedrockSpawnPercent = CFG.getInt(RootNode.NEAR_BEDROCK_BLAZE_SPAWN_PERCENT);
    }

    /** FEATURE
     * When an Entity spawns,
     * handles all the extra spawns for Blazes in the OverWorld and Nether
     */
    @EventHandler(priority = EventPriority.LOW)
    public void onEntitySpawn(CreatureSpawnEvent event) {
        Location location = event.getLocation();
        World world = location.getWorld();

        LivingEntity entity = event.getEntity();
        EntityType entityType = entity.getType();

        // FEATURE: more blazes in nether
        if (entityType == EntityType.ZOMBIFIED_PIGLIN) {
            assert world != null;
            if (world.getEnvironment() == World.Environment.NETHER) {
                if (plugin.random(bonusNetherBlazeSpawnPercent)) {
                    event.setCancelled(true);
                    entityType = EntityType.BLAZE;

                    // FEATURE: magma cubes spawn with blazes
                    if (plugin.random(bonusNetherBlazeSpawnPercent)) {
                        MagmaCube cube = (MagmaCube) (EntityHelper.spawn(location, EntityType.MAGMA_CUBE));
                        assert cube != null;
                        cube.setSize(1);
                    }
                    EntityHelper.spawn(location, entityType);
                }
            }
        }

        // FEATURE: blazes near bedrock
        if (entityType == EntityType.SKELETON) {
            assert world != null;
            if (world.getEnvironment() == World.Environment.NORMAL && location.getBlockY() < 20 && !EntityHelper.isMarkedAsOurs(entity)) {
                if (plugin.random(nearBedrockSpawnPercent)) {
                    event.setCancelled(true);
                    entityType = EntityType.BLAZE;
                    EntityHelper.spawn(location, entityType);
                    //TODO EhmBlazeSpawnEvent (OverWorld)
                }
            }
        }
    }


    /** FEATURE
     * When a Blaze dies,
     * exlode in OverWorld, multiply in the Netherm
     * drop extra loot in Nether
     */
    @EventHandler(priority = EventPriority.NORMAL)
    public void onEntityDeath(EntityDeathEvent event) {
        if (!(event.getEntity() instanceof Blaze)) {
            return;
        }
        LivingEntity entity = event.getEntity();
        World world = entity.getWorld();

        // FEATURE: nether blazes drop extra loot (glowstone and gunpowder)
        if (!EntityHelper.isLootLess(entity)) {
            if (world.getEnvironment() == World.Environment.NETHER) {
                int random = plugin.getRandom().nextInt(4);
                if (random == 0) {
                    event.getDrops().add(new ItemStack(Material.GUNPOWDER, 1));
                } else if (random == 1) {
                    event.getDrops().add(new ItemStack(Material.GLOWSTONE_DUST, 1));
                }
            } else { // no drops in the normal world (restricting blaze rods to the nether)
                event.getDrops().clear();
            }
        }

        // FEATURE: blazes explode on death in normal world
        if (world.getEnvironment() == World.Environment.NORMAL) {
            //Label explosion as creeper
            Creeper creeper = world.spawn(entity.getLocation(), Creeper.class);
            creeper.remove();
            new CreateExplosionTask(plugin, entity.getLocation(), ExplosionType.OVERWORLD_BLAZE, creeper).run(); // equal to a TNT blast, sets fires
            // fire a fireball straight up in normal worlds
            Fireball fireball = (Fireball) world.spawnEntity(entity.getLocation(), EntityType.FIREBALL);
            fireball.setDirection(new Vector(0, 10, 0));
            fireball.setYield(1.0F);
        }
    }

    /** FEATURE
     * When an Entity takes damage
     * Magmacubes turn into blazes, Blazes drop fire when hit,
     */
    @EventHandler(priority = EventPriority.LOWEST)
    public void onEntityDamage(EntityDamageEvent event) {
        Entity entity = event.getEntity();
        EntityType entityType = entity.getType();
        World world = entity.getWorld();

        // FEATURE: magma cubes become blazes when they take damage
        if (entityType == EntityType.MAGMA_CUBE && !entity.isDead() && EntityHelper.hasNoFlagIgnore(entity)) {
            //Magmacube gets replaced by blaze
            entity.remove();
            EntityHelper.spawn(entity.getLocation().add(0.0, 2.0, 0.0), EntityType.BLAZE); // replace with blaze

            //Explosion labeled as fireball
            Fireball ball = world.spawn(entity.getLocation(), Fireball.class);
            ball.remove();
            new CreateExplosionTask(plugin, entity.getLocation(), ExplosionType.MAGMACUBE_FIRE, ball).run(); // fiery explosion for effect
        }

        // FEATURE: blazes drop fire on hit, also in nether
        if (entityType == EntityType.BLAZE) {
            Blaze blaze = (Blaze) entity;
            if (blaze.getHealth() > blaze.getMaxHealth() / 2) {
                Block block = entity.getLocation().getBlock();
                Block underBlock = block.getRelative(BlockFace.DOWN);

                for (int i = 0; i < 50; i++) {
                    if (underBlock.getType() == Material.AIR) {
                        underBlock = underBlock.getRelative(BlockFace.DOWN);
                    } else break;
                }

                block = underBlock.getRelative(BlockFace.UP);
                if (block.getType() == Material.AIR && underBlock.getType() != Material.AIR && !underBlock.isLiquid() && underBlock.getY() > 0) {
                    block.setType(Material.FIRE);
                }
            }
        }
    }
}