package com.extrahardmode.features.monsters;

import com.extrahardmode.ExtraHardMode;
import com.extrahardmode.config.ExplosionType;
import com.extrahardmode.config.RootConfig;
import com.extrahardmode.config.RootNode;
import com.extrahardmode.module.EntityHelper;
import com.extrahardmode.service.ListenerModule;
import com.extrahardmode.task.CreateExplosionTask;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.PotionSplashEvent;

/**
 * All the changes to Witches
 * including:
 * New Attacks like Explosion potions, spawning of zombies
 */
public class Witches extends ListenerModule {

    private int witchSpawnPercent;
    private boolean additionalAttacks;

    public Witches(ExtraHardMode plugin)
    {
        super(plugin);
    }


    @Override
    public void starting() {
        super.starting();
        RootConfig CFG = plugin.getModuleForClass(RootConfig.class);
        witchSpawnPercent = CFG.getInt(RootNode.BONUS_WITCH_SPAWN_PERCENT);
        additionalAttacks = CFG.getBoolean(RootNode.WITCHES_ADDITIONAL_ATTACKS);
    }


    /** FEATURE
     * When an Entity spawns: Spawn a Witch above ground sometimes instead of a Zombie
     */
    @EventHandler(priority = EventPriority.LOW)
    public void onEntitySpawn(CreatureSpawnEvent event) {
        LivingEntity entity = event.getEntity();
        if (EntityHelper.isMarkedAsOurs(entity))
            return;
        Location location = event.getLocation();
        EntityType entityType = entity.getType();

        if (entityType == EntityType.ZOMBIE
                && entity.getLocation().getBlock().getRelative(BlockFace.DOWN).getType() == Material.GRASS
                && event.getSpawnReason() == CreatureSpawnEvent.SpawnReason.NATURAL) {
            if (plugin.random(witchSpawnPercent)) {
                event.setCancelled(true);
                EntityHelper.spawn(location, EntityType.WITCH);
            }
        }
    }


    /** FEATURE
     * When a potion breaks When Witches throw a potion we sometimes spawn explosions or monsters
     */
    @EventHandler(priority = EventPriority.LOW)
    public void onPotionSplash(PotionSplashEvent event) {
        ThrownPotion potion = event.getPotion();
        Location location = potion.getLocation();

        // FEATURE: enhanced witches. they throw wolf spawner and teleport potions as well as poison potions
        if (additionalAttacks && potion.getShooter() != null && EntityHelper.shooterType(potion) == EntityType.WITCH) {
            Witch witch = (Witch) potion.getShooter();

            int random = plugin.getRandom().nextInt(100);

            boolean makeExplosion = false;

            // 30% summon zombie
            if (random < 30) {
                event.setCancelled(true);

                ZombieVillager zombie = (ZombieVillager) EntityHelper.spawn(location, EntityType.ZOMBIE_VILLAGER);
                assert zombie != null;
                zombie.setBaby(true);
                if (zombie.getTarget() != null) {
                    zombie.setTarget(witch.getTarget());
                }
                EntityHelper.markLootLess(plugin, zombie);

            } else if (random < 60) {
                event.setCancelled(true);
                witch.teleport(location);
            } else if (random < 90) {
                event.setCancelled(true);
                makeExplosion = true;
            } else {
                // otherwise poison potion (selective target)
                for (LivingEntity target : event.getAffectedEntities()) {
                    if (target.getType() != EntityType.PLAYER) {
                        event.setIntensity(target, 0.0);
                    }
                }
            }

            // if explosive potion, direct damage to players in the area
            if (makeExplosion) {
                // explosion just for show, no damage
                new CreateExplosionTask(plugin, location, ExplosionType.EFFECT, null).run();

                for (LivingEntity target : event.getAffectedEntities()) {
                    if (target.getType() == EntityType.PLAYER) {
                        target.damage(3);
                    }
                }
            }
        }
    }
}
