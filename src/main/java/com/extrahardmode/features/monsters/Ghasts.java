package com.extrahardmode.features.monsters;


import com.extrahardmode.ExtraHardMode;
import com.extrahardmode.config.RootConfig;
import com.extrahardmode.config.RootNode;
import com.extrahardmode.service.ListenerModule;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;

import java.util.List;

/**
 * All changes to Ghasts including:
 * Increase loot for Ghasts drastically Ghasts don't take damage from arrows
 */
public class Ghasts extends ListenerModule {

    boolean ghastDeflectArrows;
    int ghastExpMupliplier;
    int ghastDropsMultiplier;
    int arrowDamagePercent;


    public Ghasts(ExtraHardMode plugin) {
        super(plugin);
    }

    @Override
    public void starting() {
        super.starting();
        RootConfig CFG = plugin.getModuleForClass(RootConfig.class);

        ghastDeflectArrows = CFG.getInt(RootNode.GHASTS_DEFLECT_ARROWS) != 100;
        ghastExpMupliplier = CFG.getInt(RootNode.GHASTS_EXP_MULTIPLIER);
        ghastDropsMultiplier = CFG.getInt(RootNode.GHASTS_DROPS_MULTIPLIER);
        arrowDamagePercent = CFG.getInt(RootNode.GHASTS_DEFLECT_ARROWS);
    }


    /**
     * When an Entity dies
     * Increase loot for Ghasts drastically
     */
    @EventHandler
    public void onEntityDeath(EntityDeathEvent event) {
        LivingEntity entity = event.getEntity();

        // FEATURE: ghasts deflect arrows and drop extra loot and exp
        if (entity instanceof Ghast) {
            if (ghastDeflectArrows) {
                event.setDroppedExp(event.getDroppedExp() * ghastExpMupliplier);
                List<ItemStack> itemDrops = event.getDrops();
                for (ItemStack itemDrop : itemDrops) {
                    itemDrop.setAmount(itemDrop.getAmount() * ghastDropsMultiplier);
                }
            }
        }
    }


    /** FEATURE
     * When an Entity takes damage
     * ghasts deflect arrows and drop extra loot
     */
    @EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
    public void onEntityDamage(EntityDamageByEntityEvent event) {
        Entity entity = event.getEntity();

        if (entity instanceof Ghast) {
        // FEATURE: ghasts deflect arrows and drop extra loot
            if (arrowDamagePercent < 100) {
                // only ghasts, and only if damaged by another entity (as opposed to
                // environmental damage)
                Entity damageSource = event.getDamager();

                // only arrows
                if (damageSource instanceof Arrow) {
                    // check permissions when it's shot by a player
                    event.setDamage(event.getDamage() * arrowDamagePercent / 100);
                }
            }
        }
    }
}
