package com.extrahardmode.features.monsters;

import com.extrahardmode.ExtraHardMode;
import com.extrahardmode.config.RootConfig;
import com.extrahardmode.config.RootNode;
import com.extrahardmode.module.EntityHelper;
import com.extrahardmode.service.ListenerModule;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.CreatureSpawnEvent;

/**
 * Changes to Spiders include:
 * More spiders in caves
 */
public class Spiders extends ListenerModule {

    int spiderBonusSpawnPercent;

    public Spiders(ExtraHardMode plugin) {
        super(plugin);
    }

    @Override
    public void starting() {
        super.starting();
        RootConfig CFG = plugin.getModuleForClass(RootConfig.class);
        spiderBonusSpawnPercent = CFG.getInt(RootNode.BONUS_UNDERGROUND_SPIDER_SPAWN_PERCENT);
    }

    /** FEATURE
     * When a creature spawns
     * More spiders in caves
     */
    @EventHandler(priority = EventPriority.LOW)
    public void onEntitySpawn(CreatureSpawnEvent event) {
        LivingEntity entity = event.getEntity();
        if (EntityHelper.isMarkedAsOurs(entity))
            return;
        Location location = event.getLocation();
        World world = location.getWorld();
        EntityType entityType = entity.getType();

        // FEATURE: more spiders underground
        if (entityType == EntityType.ZOMBIE) {
            assert world != null;
            if (world.getEnvironment() == World.Environment.NORMAL && location.getBlockY() < world.getSeaLevel() - 5
                    && event.getSpawnReason() == CreatureSpawnEvent.SpawnReason.NATURAL) { //Don't change type from respawned zombies etc.
                if (plugin.random(spiderBonusSpawnPercent)) {
                    event.setCancelled(true);
                    entityType = EntityType.SPIDER;
                    EntityHelper.spawn(location, entityType);
                }
            }
        }
    }
}
