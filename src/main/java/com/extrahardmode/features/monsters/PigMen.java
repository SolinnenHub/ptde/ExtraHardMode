package com.extrahardmode.features.monsters;

import com.extrahardmode.ExtraHardMode;
import com.extrahardmode.config.RootConfig;
import com.extrahardmode.config.RootNode;
import com.extrahardmode.service.ListenerModule;
import org.bukkit.Material;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.HashSet;

/**
 * Changes to ZombiePigmen including:
 * Always angry, drop netherwart on death
 */
public class PigMen extends ListenerModule {

    int pigWartDropPercent;
    private final HashSet<EntityType> piglinTypes = new HashSet<>(Arrays.asList(
            EntityType.PIGLIN,
            EntityType.PIGLIN_BRUTE,
            EntityType.ZOMBIFIED_PIGLIN
    ));

    public PigMen(ExtraHardMode plugin) {
        super(plugin);
    }

    @Override
    public void starting() {
        super.starting();
        RootConfig CFG = plugin.getModuleForClass(RootConfig.class);
        pigWartDropPercent = CFG.getInt(RootNode.NETHER_PIGS_DROP_WART);
    }

    /** FEATURE
     * When an Entity dies
     * Drop netherwart
     */
    @EventHandler(priority = EventPriority.NORMAL)
    public void onEntityDeath(EntityDeathEvent event) {
        LivingEntity entity = event.getEntity();
        if (piglinTypes.contains(entity.getType())) {
            if (pigWartDropPercent > 0 && plugin.random(pigWartDropPercent)) {
                event.getDrops().add(new ItemStack(Material.NETHER_WART));
            }
        }
    }

    /** FEATURE
     * When an Entity spawns
     * Makes ZOMBIFIED_PIGLIN always angry
     */
    @EventHandler(priority = EventPriority.NORMAL)
    public void onEntitySpawn(CreatureSpawnEvent event) {
        LivingEntity entity = event.getEntity();
        if (entity.getType().equals(EntityType.ZOMBIFIED_PIGLIN)) {
            PigZombie pigZombie = (PigZombie) entity;
            pigZombie.setAnger(Integer.MAX_VALUE);
        }
    }

    /** FEATURE
     * when a chunk loads
     * Makes ZOMBIFIED_PIGLIN always angry
     */
    @EventHandler(priority = EventPriority.LOW)
    public void onChunkLoad(ChunkLoadEvent event) {
        for (Entity entity : event.getChunk().getEntities()) {
            if (entity.getType().equals(EntityType.ZOMBIFIED_PIGLIN)) {
                PigZombie pigZombie = (PigZombie) entity;
                pigZombie.setAnger(Integer.MAX_VALUE);
            }
        }
    }
}
