package com.extrahardmode.features.monsters;

import com.extrahardmode.ExtraHardMode;
import com.extrahardmode.module.EntityHelper;
import com.extrahardmode.service.ListenerModule;
import org.bukkit.block.Biome;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.CreatureSpawnEvent;

public class CaveSpider extends ListenerModule {

    public CaveSpider(ExtraHardMode plugin) {
        super(plugin);
    }

    /**
     * When an Entity spawns: Spawn a Cave Spider sometimes instead of a spider in Swamps
     */
    @EventHandler(priority = EventPriority.LOW)
    public void onEntitySpawn(CreatureSpawnEvent event) {
        LivingEntity entity = event.getEntity();
        if (EntityHelper.isMarkedAsOurs(entity))
            return;

        Block block = entity.getLocation().getBlock();

        if (entity.getType() == EntityType.SPIDER && (block.getBiome() == Biome.SWAMP || block.getBiome() == Biome.SWAMP_HILLS)
                && event.getSpawnReason() == CreatureSpawnEvent.SpawnReason.NATURAL) {
            EntityHelper.spawn(event.getLocation(), EntityType.CAVE_SPIDER);
            event.setCancelled(true);
        }
    }
}
