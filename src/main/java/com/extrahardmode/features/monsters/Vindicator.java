package com.extrahardmode.features.monsters;

import com.extrahardmode.ExtraHardMode;

import com.extrahardmode.config.RootConfig;
import com.extrahardmode.config.RootNode;
import com.extrahardmode.module.EntityHelper;
import com.extrahardmode.service.ListenerModule;
import org.bukkit.Location;
import org.bukkit.block.Biome;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.CreatureSpawnEvent;

public class Vindicator extends ListenerModule {

    private int vindicatorSpawnPercent;

    public Vindicator(ExtraHardMode plugin) {
        super(plugin);
    }

    @Override
    public void starting() {
        super.starting();
        RootConfig CFG = plugin.getModuleForClass(RootConfig.class);
        vindicatorSpawnPercent = CFG.getInt(RootNode.BONUS_VINDICATOR_SPAWN_PERCENT);

    }

    /** FEATURE
     * When an Entity spawns: Spawn a Vindicator sometimes instead of a Skeleton
     */
    @EventHandler(priority = EventPriority.LOW)
    public void onEntitySpawn(CreatureSpawnEvent event) {
        LivingEntity entity = event.getEntity();
        if (EntityHelper.isMarkedAsOurs(entity))
            return;
        Location location = event.getLocation();
        EntityType entityType = entity.getType();

        if (entityType == EntityType.SKELETON
                && (entity.getLocation().getBlock().getBiome() == Biome.FOREST
                || entity.getLocation().getBlock().getBiome() == Biome.DARK_FOREST
                || entity.getLocation().getBlock().getBiome() == Biome.DARK_FOREST_HILLS)
                && event.getSpawnReason() == CreatureSpawnEvent.SpawnReason.NATURAL) {
            if (plugin.random(vindicatorSpawnPercent)) {
                event.setCancelled(true);
                LivingEntity vindicator = EntityHelper.spawn(location, EntityType.VINDICATOR);
                if (vindicator != null) {
                    EntityHelper.markLootLess(plugin, vindicator);
                }
            }
        }
    }
}
