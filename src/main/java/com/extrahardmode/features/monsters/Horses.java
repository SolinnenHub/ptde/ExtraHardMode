package com.extrahardmode.features.monsters;

import com.extrahardmode.ExtraHardMode;
import com.extrahardmode.service.ListenerModule;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.CreatureSpawnEvent;

public class Horses extends ListenerModule {

    public Horses(ExtraHardMode plugin) {
        super(plugin);
    }

    /** FEATURE
     * Spawn a Horses with more health
     */
    @EventHandler(priority = EventPriority.LOW)
    public void onEntitySpawn(CreatureSpawnEvent event) {
        LivingEntity entity = event.getEntity();
        if (entity.getType() == EntityType.HORSE) {
            Horse horse = (Horse) entity;
            AttributeInstance attribute = horse.getAttribute(Attribute.GENERIC_MAX_HEALTH);
            if (attribute != null) {
                double newMaxHealth = attribute.getBaseValue()*2;
                attribute.setBaseValue(newMaxHealth);
                horse.setHealth(newMaxHealth);
            }
        }
    }
}
