package com.extrahardmode.features.monsters;

import com.extrahardmode.ExtraHardMode;
import com.extrahardmode.config.RootConfig;
import com.extrahardmode.config.RootNode;
import com.extrahardmode.module.EntityHelper;
import com.extrahardmode.service.ListenerModule;
import com.extrahardmode.service.OurRandom;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * Changes to Skeletons include:
 * Immunity to arrows
 */
public class Skeletons extends ListenerModule {

    private final static String key_knockbackArrow = "ehm.skeletors.knockbackarrow";
    private final static String key_slownessArrow = "ehm.skeletors.slownessArrow";
    private final static String key_fireArrow = "ehm.skeletons.explosionArrows";
    private final static String key_spawnedMinions = "extrahardmode.skeleton.minions";
    private final static String key_totalSpawnedMinions = "extrahardmode.skeleton.minions.totalcount";
    private final static String key_parent = "extrahardmode.minion.parent";
    private final static String key_minionTag = "extrahardmode.skeleton.minion";

    private double knockBackStrength;
    private int slownessLength;
    private int fireTicks;
    private int deflectPercent;

    private boolean snowballs;
    private int snowballsPercent;

    private boolean fireworks;
    private int fireworksPercent;

    private boolean explosionArrowEnable;
    private int explosionPercent;

    private boolean silverfishEnable;
    private int silverfishPercent;
    private int currentLimit;
    private int totalLimit;

    public Skeletons(ExtraHardMode plugin) {
        super(plugin);
    }

    @Override
    public void starting() {
        super.starting();
        RootConfig CFG = plugin.getModuleForClass(RootConfig.class);

        knockBackStrength = CFG.getDouble(RootNode.SKELETONS_FIREWORK_KNOCKBACK_VEL);
        slownessLength = CFG.getInt(RootNode.SKELETONS_SNOWBALLS_SLOW_LEN);
        fireTicks = CFG.getInt(RootNode.SKELETONS_FIREBALL_PLAYER_FIRETICKS);
        deflectPercent = CFG.getInt(RootNode.SKELETONS_DEFLECT_ARROWS);

        snowballs = CFG.getBoolean(RootNode.SKELETONS_SNOWBALLS_ENABLE);
        snowballsPercent = CFG.getInt(RootNode.SKELETONS_SNOWBALLS_PERCENT);

        fireworks = CFG.getBoolean(RootNode.SKELETONS_FIREWORK_ENABLE);
        fireworksPercent = CFG.getInt(RootNode.SKELETONS_FIREWORK_PERCENT);

        explosionArrowEnable = CFG.getBoolean(RootNode.SKELETONS_FIREBALL_ENABLE);
        explosionPercent = CFG.getInt(RootNode.SKELETONS_FIREBALL_PERCENTAGE);

        silverfishEnable = CFG.getBoolean(RootNode.SKELETONS_RELEASE_SILVERFISH_ENABLE);
        silverfishPercent = CFG.getInt(RootNode.SKELETONS_RELEASE_SILVERFISH_PERCENT);
        currentLimit = CFG.getInt(RootNode.SKELETONS_RELEASE_SILVERFISH_LIMIT);
        totalLimit = CFG.getInt(RootNode.SKELETONS_RELEASE_SILVERFISH_LIMIT_TOTAL);
    }


    /** FEATURE:
     * skeletons can knock back
     *
     * When an entity takes damage
     * skeletons are immune to arrows
     */
    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerHitByArrow(EntityDamageByEntityEvent event) {
        if (event.getEntity() instanceof LivingEntity) {
            LivingEntity entity = (LivingEntity) event.getEntity();
            if (event.getDamager() instanceof Projectile && event.getEntity() instanceof Player) {
                Projectile bullet = (Projectile) event.getDamager();
                // FEATURE: skeletons can knock back
                // knock back target with half the arrow's velocity
                if (bullet.hasMetadata(key_knockbackArrow)) {
                    entity.setVelocity(bullet.getVelocity().multiply(knockBackStrength));
                } else if (bullet.hasMetadata(key_slownessArrow)) {
                    entity.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, slownessLength, 3));
                } else if (bullet.hasMetadata(key_fireArrow)) {
                    //Allow for a variable amount of fireticks
                    int ticksBefore = entity.getFireTicks() >= 100 ? entity.getFireTicks() - 100 : 0; //fireticks from the arrow are already applied
                    entity.setFireTicks(ticksBefore + fireTicks);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onSkeliDamagedByArrow(EntityDamageByEntityEvent event) {
        Entity entity = event.getEntity();

        // FEATURE: arrows pass through skeletons
        if (entity.getType().equals(EntityType.SKELETON) && deflectPercent > 0) {
            Entity damageSource = event.getDamager();

            // only arrows
            if (damageSource.getType().equals(EntityType.ARROW)) {
                Arrow arrow = (Arrow) damageSource;

                // cancel the damage
                event.setCancelled(true);

                // teleport the arrow a single block farther along its flight path, note that .6 and 12 were the unexplained recommended values, for speed and spread, reflectively, in the bukkit wiki
                arrow.remove();
                entity.getWorld().spawnArrow(arrow.getLocation().add((arrow.getVelocity().normalize()).multiply(2)), arrow.getVelocity(), 0.6f, 12.0f);
            }
        }
    }


    /**
     * when an entity shoots a bow...
     * skeletons shoot silverfish
     */
    @EventHandler(priority = EventPriority.NORMAL)
    public void onShootProjectile(ProjectileLaunchEvent event) {
        Location location = event.getEntity().getLocation();
        World world = location.getWorld();
        if (world == null) {
            return;
        }

        // FEATURE: skeletons sometimes release silverfish to attack their targets
        if (event.getEntity() instanceof Arrow && event.getEntity().getShooter() instanceof Skeleton) {
            Arrow arrow = (Arrow) event.getEntity();
            Skeleton skeleton = (Skeleton) event.getEntity().getShooter();
            //Slowness Arrows
            if (snowballs && OurRandom.percentChance(snowballsPercent)) {
                arrow.setMetadata(key_slownessArrow, new FixedMetadataValue(plugin, true));
                Snowball snowball = world.spawn(arrow.getLocation(), Snowball.class);
                snowball.setShooter(arrow.getShooter());
                snowball.setVelocity(arrow.getVelocity());
            }
            //Arrow can have multiple effects
            //Knockback Arrows
            else if (fireworks && OurRandom.percentChance(fireworksPercent)) {
                arrow.setMetadata(key_knockbackArrow, new FixedMetadataValue(plugin, true));
                Firework peng = world.spawn(arrow.getLocation(), Firework.class);
                peng.setVelocity(arrow.getVelocity());
            }
            //Explosion arrows
            else if (explosionArrowEnable && OurRandom.percentChance(explosionPercent)) {
                arrow.setMetadata(key_fireArrow, new FixedMetadataValue(plugin, true));
                SmallFireball fireball = world.spawn(arrow.getLocation(), SmallFireball.class);
                fireball.setVelocity(arrow.getVelocity());
                //Silverfish
            } else if (skeleton != null && skeleton.getTarget() instanceof Player && silverfishEnable && OurRandom.percentChance(silverfishPercent)) { // To prevent tons of Silverfish
                //respect summoning limits
                if (getMinionsSpawnedBySkeli(skeleton, plugin).size() < currentLimit
                        && getTotalMinionsSummonedBySkeli(skeleton) < totalLimit) {
                    // replace arrow with silverfish
                    event.setCancelled(true);

                    // replace with silverfish, quarter velocity of arrow, wants to attack same target as skeleton
                    Creature silverFish = (Creature) skeleton.getWorld().spawnEntity(skeleton.getLocation().add(0.0, 1.5, 0.0), EntityType.SILVERFISH);
                    silverFish.setVelocity(arrow.getVelocity().multiply(0.25));
                    silverFish.setTarget(skeleton.getTarget());

                    EntityHelper.markLootLess(plugin, silverFish); // this silverfish doesn't drop loot
                    setMinion(plugin, silverFish);
                    setParentOfMinion(skeleton, silverFish, plugin);
                    addMinionToSkeli(skeleton, silverFish, plugin);
                }
            }
        }
    }

    /** When a skeleton dies kill all the spawned silverfish as well */
    @EventHandler(priority = EventPriority.NORMAL)
    public void onSkeletonDeath(EntityDeathEvent event) {
        if (event.getEntity().getType() .equals(EntityType.SKELETON)) {
            //Kill all silverfish, but do it slowly as if they are burning up
            for (LivingEntity silverfish : event.getEntity().getWorld().getLivingEntities()) {
                if (isMinion(silverfish)) {
                    for (UUID id : getMinionsSpawnedBySkeli(event.getEntity(), plugin)) {
                        if (silverfish.getUniqueId() == id) {
                            silverfish.setFireTicks(Integer.MAX_VALUE);
                            //new SlowKillTask(silverfish, plugin);
                        }
                    }
                }
            }
        }
    }


    @EventHandler
    public void onMinionDeath(EntityDeathEvent event) {
        //Remove the silverfish from the spawnned list of silverfish in the skeli
        LivingEntity entity = event.getEntity();
        if (isMinion(entity)) {
            UUID parent = getParentOfMinion(entity);

            //Try to find the parent by id
            for (LivingEntity worldEntity : entity.getWorld().getLivingEntities()) {
                if (worldEntity.getUniqueId() == parent)
                    removeMinionFromSkeli(entity.getUniqueId(), worldEntity);
            }
        }
    }


    /**
     * Add a minion to the list of minions spawned by this entity
     *
     * @param summoner entity that summoned another entiry
     * @param minion   minion that has been summoned
     * @param plugin   owning plugin for metadata
     */
    @SuppressWarnings("unchecked")
    public static void addMinionToSkeli(LivingEntity summoner, LivingEntity minion, Plugin plugin) {
        List<UUID> idList = new ArrayList<>(1);
        //Get minions already set and append the new Minion
        List<MetadataValue> meta = summoner.getMetadata(key_spawnedMinions);
        for (MetadataValue val : meta)
            if (val.getOwningPlugin() == plugin)
                if (val.value() instanceof List)
                    idList = (List<UUID>) val.value();
        assert idList != null;
        idList.add(minion.getUniqueId());
        summoner.setMetadata(key_spawnedMinions, new FixedMetadataValue(plugin, idList));

        //Increment the total count of minions summoned
        int totalCount = getTotalMinionsSummonedBySkeli(summoner);
        totalCount++;
        summoner.setMetadata(key_totalSpawnedMinions, new FixedMetadataValue(plugin, totalCount));
    }


    /**
     * Remove the minion from the list of summoned minions
     *
     * @param minionId id of the minion
     * @param summoner the entity that summoned the minion
     */
    @SuppressWarnings("unchecked")
    public static void removeMinionFromSkeli(UUID minionId, LivingEntity summoner) {
        List<MetadataValue> meta = summoner.getMetadata(key_spawnedMinions);
        if (!meta.isEmpty()) {
            MetadataValue value = meta.get(0);
            if (value.value() instanceof List) {
                ((List<UUID>) Objects.requireNonNull(value.value())).removeIf(uuid -> minionId == uuid);
            }
        }
    }


    /**
     * Get all the Ids of the Minions spawned by this CustomSkeleton
     *
     * @param entity entity to get the minions for
     * @param plugin owning plugin to access meta
     *
     * @return Unique ids of all minions that have been spawned (minions can be dead)
     */
    @SuppressWarnings("unchecked")
    public static List<UUID> getMinionsSpawnedBySkeli(LivingEntity entity, Plugin plugin) {
        List<MetadataValue> meta = entity.getMetadata(key_spawnedMinions);
        List<UUID> ids = new ArrayList<>(meta.size());
        for (MetadataValue val : meta)
            if (val.getOwningPlugin() == plugin)
                if (val.value() instanceof List)
                    ids = (List<UUID>) val.value();
        return ids;
    }


    /**
     * Get the total number of minions summoned by this entity
     *
     * @param entity entity to get minion count
     * @return count or 0 if not set
     */
    public static int getTotalMinionsSummonedBySkeli(LivingEntity entity) {
        List<MetadataValue> meta = entity.getMetadata(key_totalSpawnedMinions);
        int totalCount = 0;
        if (!meta.isEmpty()) {
            MetadataValue value = meta.get(0);
            if (value.value() instanceof Integer) {
                totalCount = (Integer) value.value();
            }
        }
        return totalCount;
    }


    public static void setMinion(Plugin plugin, LivingEntity entity) {
        entity.setMetadata(key_minionTag, new FixedMetadataValue(plugin, true));
    }


    public static boolean isMinion(LivingEntity entity) {
        return entity.hasMetadata(key_minionTag);
    }


    /**
     * Set the parent that summoned this minion
     *
     * @param summoner parent summoner
     * @param minion   summoned minion
     * @param owning   plugin that spawned the minion
     */
    public static void setParentOfMinion(LivingEntity summoner, LivingEntity minion, Plugin owning) {
        minion.setMetadata(key_parent, new FixedMetadataValue(owning, summoner.getUniqueId()));
    }


    /**
     * Get the parent that summoned the minion
     *
     * @param minion minion to get the parent for
     * @return id of parent or id of minion if parent not set
     */
    public static UUID getParentOfMinion(LivingEntity minion) {
        List<MetadataValue> meta = minion.getMetadata(key_parent);
        if (!meta.isEmpty()) {
            MetadataValue metaVal = meta.get(0);
            if (metaVal.value() instanceof UUID)
                return (UUID) metaVal.value();
        }
        return minion.getUniqueId();
    }
}