package com.extrahardmode.features.monsters;

import com.extrahardmode.ExtraHardMode;

import com.extrahardmode.config.RootConfig;
import com.extrahardmode.config.RootNode;
import com.extrahardmode.module.EntityHelper;
import com.extrahardmode.service.ListenerModule;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.CreatureSpawnEvent;


public class Vex extends ListenerModule {

    int vexSpawnPercent;

    public Vex(ExtraHardMode plugin)
    {
        super(plugin);
    }

    @Override
    public void starting() {
        super.starting();
        RootConfig CFG = plugin.getModuleForClass(RootConfig.class);
        vexSpawnPercent = CFG.getInt(RootNode.BONUS_VEX_SPAWN_PERCENT);
    }

    /** FEATURE
     * When an Entity spawns: Spawn a Vex sometimes instead of a bat
     */
    @EventHandler(ignoreCancelled = true, priority = EventPriority.LOW)
    public void onEntitySpawn(CreatureSpawnEvent event) {
        LivingEntity entity = event.getEntity();
        if (EntityHelper.isMarkedAsOurs(entity))
            return;

        if (entity.getType() == EntityType.BAT && entity.getLocation().getBlockY() > 50 && event.getSpawnReason() == CreatureSpawnEvent.SpawnReason.NATURAL) {
            if (plugin.random(vexSpawnPercent)) {
                event.setCancelled(true);
                EntityHelper.spawn(event.getLocation(), EntityType.VEX);
            }
        }
    }
}
