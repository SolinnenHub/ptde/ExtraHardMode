package com.extrahardmode.features.monsters;

import com.extrahardmode.ExtraHardMode;
import com.extrahardmode.config.ExplosionType;
import com.extrahardmode.config.RootConfig;
import com.extrahardmode.config.RootNode;
import com.extrahardmode.module.EntityHelper;
import com.extrahardmode.module.PlayerModule;
import com.extrahardmode.service.ListenerModule;
import com.extrahardmode.task.CreateExplosionTask;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;

/**
 * Changes to Creepers including:
 * Naturally spawning Charged Creepers, Charged Creepers exploding on hit.
 */
public class BumBumBens extends ListenerModule {

    private PlayerModule playerModule;

    int chargedSpawnPercent;

    public BumBumBens(ExtraHardMode plugin)
    {
        super(plugin);
    }


    @Override
    public void starting() {
        super.starting();
        RootConfig CFG = plugin.getModuleForClass(RootConfig.class);
        playerModule = plugin.getModuleForClass(PlayerModule.class);

        chargedSpawnPercent = CFG.getInt(RootNode.CHARGED_CREEPER_SPAWN_PERCENT);
    }

    /** FEATURE
     * naturally spawning Charged Creepers
     */
    @EventHandler(priority = EventPriority.LOW)
    public void onEntitySpawn(CreatureSpawnEvent event) {
        LivingEntity entity = event.getEntity();
        if (EntityHelper.isMarkedAsOurs(entity))
            return;
        EntityType entityType = entity.getType();

        if (entityType == EntityType.CREEPER) {
            if (plugin.random(chargedSpawnPercent)) {
                ((Creeper) entity).setPowered(true);
            }
        }
    }

    /** FEATURE
     * Charged creepers explode on hit, burning creepers will cause a big explosion
     */
    @EventHandler(priority = EventPriority.LOWEST)
    public void onEntityDamage(EntityDamageEvent event) {
        Entity entity = event.getEntity();
        EntityType entityType = entity.getType();

        // is this an entity damaged by entity event?
        EntityDamageByEntityEvent damageByEntityEvent = null;
        if (event instanceof EntityDamageByEntityEvent) {
            damageByEntityEvent = (EntityDamageByEntityEvent) event;
        }

        // FEATURE: charged creepers explode on hit
        if ((entityType == EntityType.CREEPER) && !entity.isDead()) {
            Creeper creeper = (Creeper) entity;
            if (creeper.isPowered()) {
                Player damager = null;
                //Always explode when damaged by a player
                if (damageByEntityEvent != null) {
                    if (damageByEntityEvent.getDamager().getType().equals(EntityType.PLAYER)) { // Normal Damage from a player
                        damager = (Player) damageByEntityEvent.getDamager();
                        if (playerModule.playerBypasses(damager)) {
                            return;
                        }
                    } else if (damageByEntityEvent.getDamager() instanceof Projectile) { // Damaged by an arrow shot by a player
                        Projectile bullet = (Projectile) damageByEntityEvent.getDamager();
                        if (bullet.getShooter() instanceof Player) { //otherwise skeli/dispenser etc.
                            damager = (Player) bullet.getShooter();
                        }
                        if (damager != null && playerModule.playerBypasses(damager)) {
                            return;
                        }
                    }
                }
                if (creeper.getTarget() == null && damager == null) { // If not targeting a player this is an explosion we don't need. Trying to prevent unnecessary world damage
                    return;
                }
                EntityHelper.markLootLess(plugin, (LivingEntity) entity);
                entity.remove();
                new CreateExplosionTask(plugin, entity.getLocation(), ExplosionType.CREEPER_CHARGED, entity).run();
            }
        }
    }


    /**
     * When something explodes
     * Increase size of Creeper explosions
     */
    @EventHandler(priority = EventPriority.HIGH)
    //give some time for other plugins to block the event
    public void onExplosion(EntityExplodeEvent event) {
        Entity entity = event.getEntity();

        // FEATURE: bigger creeper explosions (for more-frequent cave-ins)
        // Charged creeper explosion is handled in onEntityDamage
        if (entity.getType().equals(EntityType.CREEPER)) {
            event.setCancelled(true);
            EntityHelper.flagIgnore(plugin, entity);//Ignore this creeper in further calls to this method
            if (((Creeper) entity).isPowered()) {
                new CreateExplosionTask(plugin, entity.getLocation(), ExplosionType.CREEPER_CHARGED, entity).run();
            } else { //normal creeper
                new CreateExplosionTask(plugin, entity.getLocation(), ExplosionType.CREEPER, entity).run();
            }
        }
    }
}
