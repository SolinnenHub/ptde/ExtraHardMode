package com.extrahardmode.features.monsters;

import com.extrahardmode.ExtraHardMode;

import com.extrahardmode.config.RootConfig;
import com.extrahardmode.config.RootNode;
import com.extrahardmode.module.EntityHelper;
import com.extrahardmode.service.ListenerModule;
import org.bukkit.block.Biome;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.CreatureSpawnEvent;

public class Guardians extends ListenerModule {

    int guardiansSpawnPercent;

    public Guardians(ExtraHardMode plugin) {
        super(plugin);
    }

    @Override
    public void starting() {
        super.starting();
        RootConfig CFG = plugin.getModuleForClass(RootConfig.class);
        guardiansSpawnPercent = CFG.getInt(RootNode.BONUS_GUARDIANS_SPAWN_PERCENT);
    }

    /** FEATURE
     * When an Entity spawns: Spawn a Guardians sometimes instead of a Squid
     */
    @EventHandler(priority = EventPriority.LOW)
    public void onEntitySpawn(CreatureSpawnEvent event) {
        LivingEntity entity = event.getEntity();
        if (entity.getType() == EntityType.SQUID) {
            if (EntityHelper.isMarkedAsOurs(entity)) {
                return;
            }
            if (guardiansSpawnPercent == 0) {
                return;
            }

            if (entity.getLocation().getBlock().getBiome() == Biome.DEEP_OCEAN
                    || entity.getLocation().getBlock().getBiome() == Biome.OCEAN) {
                if (plugin.random(guardiansSpawnPercent)) {
                    event.setCancelled(true);
                    EntityHelper.spawn(event.getLocation(), EntityType.GUARDIAN);
                }
            }
        }
    }
}
