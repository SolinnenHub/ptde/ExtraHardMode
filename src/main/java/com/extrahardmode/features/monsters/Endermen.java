package com.extrahardmode.features.monsters;

import com.extrahardmode.ExtraHardMode;
import com.extrahardmode.service.ListenerModule;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Enderman;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import java.util.Objects;


/**
 * Changes to Enderman including:
 * Teleportation of the Player towards the Enderman ,
 */
public class Endermen extends ListenerModule {

    public Endermen(ExtraHardMode plugin) {
        super(plugin);
    }


    /**
     * when an entity (not a player) teleports...
     */
    @EventHandler(ignoreCancelled = true)
    public void onEntityTeleport(EntityTeleportEvent event) {
        Entity entity = event.getEntity();
        World world = entity.getWorld();

        if (entity instanceof Enderman) {
            Enderman enderman = (Enderman) entity;

            // ignore endermen which aren't fighting players
            if (!(enderman.getTarget() instanceof Player))
                return;

            // ignore endermen which are taking damage from the environment (to avoid rapid teleportation due to rain or suffocation)
            if (enderman.getLastDamageCause() != null && enderman.getLastDamageCause().getCause() != EntityDamageEvent.DamageCause.ENTITY_ATTACK)
                return;

            Player player = (Player) enderman.getTarget();

            // ignore when player is in a different world from the enderman
            if (!player.getWorld().equals(enderman.getWorld()))
                return;

            // half the time, teleport the player instead
            if (plugin.random(50)) {
                int distanceSquared = (int) player.getLocation().distanceSquared(enderman.getLocation());

                // play sound at old location
                world.playSound(player.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, 1.0F, 1.0F);
                Block destinationBlock;

                // if the player is far away
                if (distanceSquared > 75) {
                    // have the enderman swap places with the player
                    destinationBlock = enderman.getLocation().getBlock();
                    enderman.teleport(player.getLocation());
                }

                // otherwise if the player is close
                else {
                    // teleport the player to the enderman's destination
                    destinationBlock = Objects.requireNonNull(event.getTo()).getBlock();
                }

                while (destinationBlock.getType() != Material.AIR || destinationBlock.getRelative(BlockFace.UP).getType() != Material.AIR
                        && destinationBlock.getY() < destinationBlock.getWorld().getMaxHeight())
                    destinationBlock = destinationBlock.getRelative(BlockFace.UP);


                int playerY = player.getLocation().getBlockY(), destY = destinationBlock.getLocation().getBlockY();

                //Sometimes enderman will teleport at/below bedrock into the void? See issue #165
                if (destY < 3)
                    return;

                //Limit the height difference so players arent teleported into caves or teleported out of caves etc.
                if (Math.abs(playerY - destY) > 10)
                    return;

                //Prevent Enderman from loosing aggro because player got ported into water
                Material underType = destinationBlock.getRelative(BlockFace.DOWN).getType();
                if (underType == Material.WATER)
                    return;

                player.teleport(destinationBlock.getLocation(), PlayerTeleportEvent.TeleportCause.ENDER_PEARL);

                // play sound at new location
                world.playSound(player.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, 1.0F, 1.0F);
                event.setCancelled(true);
            }
        }
    }
}
