package com.extrahardmode.service;

import com.extrahardmode.ExtraHardMode;

public abstract class EHMModule implements IModule {

    protected final ExtraHardMode plugin;

    protected EHMModule(ExtraHardMode plugin) {
        this.plugin = plugin;
    }
}
