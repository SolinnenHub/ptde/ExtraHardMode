package com.extrahardmode.service;


import com.extrahardmode.ExtraHardMode;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

public class ListenerModule implements IModule, Listener {

    protected final ExtraHardMode plugin;

    public ListenerModule(ExtraHardMode plugin) {
        this.plugin = plugin;
    }

    @Override
    public void starting() {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @Override
    public void closing() {
        HandlerList.unregisterAll(this);
    }
}
