package com.extrahardmode.service.config;

public interface ConfigNode {

    /**
     * Get the config path.
     *
     * @return Config path.
     */
    String getPath();

    /**
     * Get the variable type.
     *
     * @return Variable type.
     */
    VarType getVarType();

    /**
     * Get the SubType Used especially for verfification
     *
     * @return SubType
     */
    SubType getSubType();

    /**
     * Get the default value.
     *
     * @return Default value.
     */
    Object getDefaultValue();

    /**
     * Get a value to disable this Node
     */
    Object getValueToDisable();

    /**
     * Variable Types.
     */
    enum VarType {
        STRING,
        INTEGER,
        DOUBLE,
        BOOLEAN,
        LIST,
        POTION_EFFECT,
        MATERIAL,
        @Deprecated MATERIAL_LIST,
        @Deprecated BLOCK_RELATION_LIST,
        COMMENT
    }


    /**
     * SubTypes, like percentage, y-value, custom etc
     */
    enum SubType {
        PERCENTAGE,
        Y_VALUE,
        HEALTH,
        NATURAL_NUMBER
    }
}
