package com.extrahardmode.service;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class IoHelper {

    public static void writeHeader(File input, ByteArrayOutputStream headerStream) {
        try {
            //Read original file contents -> memstream
            BufferedReader br = new BufferedReader(new FileReader(input));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null)
                sb.append(line).append(String.format("%n"));
            br.close();

            //Header -> original file
            FileOutputStream outFileStream = new FileOutputStream(input);

            //header + original file
            OutputStreamWriter memWriter = new OutputStreamWriter(headerStream, StandardCharsets.UTF_8.newEncoder());
            memWriter.write(sb.toString());
            memWriter.close();
            headerStream.writeTo(outFileStream);

            headerStream.close();
            outFileStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
