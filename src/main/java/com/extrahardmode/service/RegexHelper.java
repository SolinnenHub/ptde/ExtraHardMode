package com.extrahardmode.service;

import java.util.regex.Pattern;


/**
 * Just some regex functions I use all the time like striping invalid characters before parsing an int
 */
public class RegexHelper {
    private static final Pattern onlyNums = Pattern.compile("[^0-9]");
    private static final Pattern onlyEnum = Pattern.compile("[^A-Z_]");
    private static final Pattern containsNums = Pattern.compile(".*\\d.*");
    private static final Pattern containsLetters = Pattern.compile(".*[a-zA-Z_].*");


    public static int parseNumber(String input) throws NumberFormatException
    {
        int num;
        if (containsNumbers(input))
        {
            input = stripNumber(input);
            num = Integer.parseInt(input);
        } else
            throw new NumberFormatException("Not a readable number \"" + input + "\"");
        return num;
    }


    public static boolean containsNumbers(String str)
    {
        return containsNums.matcher(str).matches();
    }


    public static boolean containsLetters(String str)
    {
        return containsLetters.matcher(str).matches();
    }


    public static String stripEnum(String input)
    {
        input = input.toUpperCase();
        input = onlyEnum.matcher(input).replaceAll("");
        return input;
    }


    public static String stripNumber(String input)
    {
        return onlyNums.matcher(input).replaceAll("");
    }


}
