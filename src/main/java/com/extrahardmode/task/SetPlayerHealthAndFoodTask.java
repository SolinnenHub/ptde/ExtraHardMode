package com.extrahardmode.task;

import org.bukkit.entity.Player;

public class SetPlayerHealthAndFoodTask implements Runnable {

    private final Player player;
    private final int health;
    private final int food;

    public SetPlayerHealthAndFoodTask(Player player, int health, int food) {
        this.player = player;
        this.health = health;
        this.food = food;
    }

    @Override
    public void run() {
        try {
            try {
                this.player.setHealth(health);
            } catch (IllegalArgumentException ignored) {
                // if less than zero or higher than max, no changes
            }

            this.player.setFoodLevel(this.food);
        } catch (NullPointerException ignored) {
            // Catch if player is null.
        }
    }
}
