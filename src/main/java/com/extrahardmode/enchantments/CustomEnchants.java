package com.extrahardmode.enchantments;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;

public class CustomEnchants {

    public static final Enchantment STAMINA_RESTORATION = new EnchantmentWrapper("stamina_restoration",
            "Восстановление выносливости", 3, new ArrayList<>(Collections.singletonList(Material.SHIELD)));

    public static void register() {
        boolean registered = Arrays.stream(Enchantment.values()).collect(Collectors.toList()).contains(STAMINA_RESTORATION);
        if (!registered) {
            registerEnchantment(STAMINA_RESTORATION);
        }
    }

    public static void registerEnchantment(Enchantment enchantment) {
        try {
            Field f = Enchantment.class.getDeclaredField("acceptingNew");
            f.setAccessible(true);
            f.set(null, true);
            Enchantment.registerEnchantment(enchantment);
            System.out.println("[ExtraHardMode] New enchantment registered: "+enchantment.getKey());
        } catch (Exception e) {
            System.out.println("§4[ExtraHardMode] Failed to register enchantment: "+enchantment.getKey());
            e.printStackTrace();
        }
    }
}
