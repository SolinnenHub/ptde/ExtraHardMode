package com.extrahardmode.module;

import com.extrahardmode.ExtraHardMode;
import com.extrahardmode.service.EHMModule;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.*;

public class PlayerModule extends EHMModule {

    public static final HashMap<Material, Double> weights = new HashMap<>();
    public static double norm;

    public static final HashMap<UUID, Float> playersWeightPointsCache = new HashMap<>(100);

    public PlayerModule(ExtraHardMode plugin) {
        super(plugin);

        // https://darksouls.fandom.com/ru/wiki/Щит_бога_войны
        double SHIELD_W = 4.0;

        // https://darksouls.fandom.com/ru/wiki/Золотая_корона
        double TURTLE_HELMET_W = 3.5;

        // https://darksouls.fandom.com/ru/wiki/Кожаный_сет_(Dark_Souls_III)
        double LEATHER_HELMET_W = 2.3;
        double LEATHER_CHESTPLATE_W = 5.4;
        double LEATHER_LEGGINGS_W = 3.3;
        double LEATHER_BOOTS_W = 1.5;
        double LEATHER_SET = LEATHER_HELMET_W + LEATHER_CHESTPLATE_W + LEATHER_LEGGINGS_W + LEATHER_BOOTS_W;

        // https://darksouls.fandom.com/ru/wiki/Кольчужный_сет_(Dark_Souls_III)
        double CHAINMAIL_HELMET_W = 3.9;
        double CHAINMAIL_CHESTPLATE_W = 8.8;
        double CHAINMAIL_LEGGINGS_W = 5.1;
        double CHAINMAIL_BOOTS_W = 2.5;
        double CHAINMAIL_SET = CHAINMAIL_HELMET_W + CHAINMAIL_CHESTPLATE_W + CHAINMAIL_LEGGINGS_W + CHAINMAIL_BOOTS_W;

        // https://darksouls.fandom.com/ru/wiki/Сет_рыцаря_(Dark_Souls_III)
        double IRON_HELMET_W = 5.2;
        double IRON_CHESTPLATE_W = 10.8;
        double IRON_LEGGINGS_W = 6.7;
        double IRON_BOOTS_W = 3.6;
        double IRON_SET = IRON_HELMET_W + IRON_CHESTPLATE_W + IRON_LEGGINGS_W + IRON_BOOTS_W;

        // https://darksouls.fandom.com/ru/wiki/Сет_драконоборца
        double GOLDEN_HELMET_W = 5.6;
        double GOLDEN_CHESTPLATE_W = 13.8;
        double GOLDEN_LEGGINGS_W = 8.1;
        double GOLDEN_BOOTS_W = 4.1;
        double GOLDEN_SET = GOLDEN_HELMET_W + GOLDEN_CHESTPLATE_W + GOLDEN_LEGGINGS_W + GOLDEN_BOOTS_W;

        // https://darksouls.fandom.com/ru/wiki/Сет_серебряного_рыцаря_(Dark_Souls_III)
        double DIAMOND_HELMET_W = 6.1;
        double DIAMOND_CHESTPLATE_W = 15.0;
        double DIAMOND_LEGGINGS_W = 9.1;
        double DIAMOND_BOOTS_W = 4.7;
        double DIAMOND_SET = DIAMOND_HELMET_W + DIAMOND_CHESTPLATE_W + DIAMOND_LEGGINGS_W + DIAMOND_BOOTS_W;

        // https://darksouls.fandom.com/ru/wiki/Сет_Гундира
        double NETHERITE_HELMET_W = 7.6;
        double NETHERITE_CHESTPLATE_W = 18.0;
        double NETHERITE_LEGGINGS_W = 10.3;
        double NETHERITE_BOOTS_W = 6.1;
        double NETHERITE_SET = NETHERITE_HELMET_W + NETHERITE_CHESTPLATE_W + NETHERITE_LEGGINGS_W + NETHERITE_BOOTS_W;

        norm = Collections.max(Arrays.asList(
                SHIELD_W, TURTLE_HELMET_W, LEATHER_SET, CHAINMAIL_SET, IRON_SET, GOLDEN_SET, DIAMOND_SET, NETHERITE_SET
        ));

        weights.put(Material.SHIELD, SHIELD_W / norm);

        weights.put(Material.TURTLE_HELMET, TURTLE_HELMET_W / norm);

        weights.put(Material.LEATHER_HELMET, LEATHER_HELMET_W / norm);
        weights.put(Material.LEATHER_CHESTPLATE, LEATHER_CHESTPLATE_W / norm);
        weights.put(Material.LEATHER_LEGGINGS, LEATHER_LEGGINGS_W / norm);
        weights.put(Material.LEATHER_BOOTS, LEATHER_BOOTS_W / norm);

        weights.put(Material.CHAINMAIL_HELMET, CHAINMAIL_HELMET_W / norm);
        weights.put(Material.CHAINMAIL_CHESTPLATE, CHAINMAIL_CHESTPLATE_W / norm);
        weights.put(Material.CHAINMAIL_LEGGINGS, CHAINMAIL_LEGGINGS_W / norm);
        weights.put(Material.CHAINMAIL_BOOTS, CHAINMAIL_BOOTS_W / norm);

        weights.put(Material.IRON_HELMET, IRON_HELMET_W / norm);
        weights.put(Material.IRON_CHESTPLATE, IRON_CHESTPLATE_W / norm);
        weights.put(Material.IRON_LEGGINGS, IRON_LEGGINGS_W / norm);
        weights.put(Material.IRON_BOOTS, IRON_BOOTS_W / norm);

        weights.put(Material.GOLDEN_HELMET, GOLDEN_HELMET_W / norm);
        weights.put(Material.GOLDEN_CHESTPLATE, GOLDEN_CHESTPLATE_W / norm);
        weights.put(Material.GOLDEN_LEGGINGS, GOLDEN_LEGGINGS_W / norm);
        weights.put(Material.GOLDEN_BOOTS, GOLDEN_BOOTS_W / norm);

        weights.put(Material.DIAMOND_HELMET, DIAMOND_HELMET_W / norm);
        weights.put(Material.DIAMOND_CHESTPLATE, DIAMOND_CHESTPLATE_W / norm);
        weights.put(Material.DIAMOND_LEGGINGS, DIAMOND_LEGGINGS_W / norm);
        weights.put(Material.DIAMOND_BOOTS, DIAMOND_BOOTS_W / norm);

        weights.put(Material.NETHERITE_HELMET, NETHERITE_HELMET_W / norm);
        weights.put(Material.NETHERITE_CHESTPLATE, NETHERITE_CHESTPLATE_W / norm);
        weights.put(Material.NETHERITE_LEGGINGS, NETHERITE_LEGGINGS_W / norm);
        weights.put(Material.NETHERITE_BOOTS, NETHERITE_BOOTS_W / norm);
    }

    @Override
    public void starting() {}

    public boolean playerBypasses(Player player) {
        if (player == null)
            return false;

        return player.getGameMode().equals(GameMode.CREATIVE) || player.getGameMode().equals(GameMode.SPECTATOR);
    }

    /**
     * Counts the number of items of a specific type
     *
     * @param inv     to count in
     * @param toCount the Material to count
     *
     * @return the number of items as Integer
     */
    public static int countInvItem(PlayerInventory inv, Material toCount) {
        int counter = 0;
        for (ItemStack stack : inv.getContents()) {
            if (stack != null && stack.getType().equals(toCount)) {
                counter += stack.getAmount();
            }
        }
        return counter;
    }

    public static float getWeightPointsNorm(final Player player, boolean update) {
        if (playersWeightPointsCache.containsKey(player.getUniqueId())) {
            if (update) {
                float weightPointsNorm = calculateWeightPointsNorm(player);
                playersWeightPointsCache.put(player.getUniqueId(), weightPointsNorm);
                return weightPointsNorm;
            } else {
                return playersWeightPointsCache.get(player.getUniqueId());
            }
        } else {
            float weightPointsNorm = calculateWeightPointsNorm(player);
            playersWeightPointsCache.put(player.getUniqueId(), weightPointsNorm);
            return weightPointsNorm;
        }
    }

    private static float calculateWeightPointsNorm(Player player) {
        float points = 0.0F;
        PlayerInventory inv = player.getInventory();
        for (ItemStack armor : inv.getArmorContents()) {
            if (armor != null) {
                points += weights.getOrDefault(armor.getType(), 0D);
            }
        }
        for (ItemStack armor : inv.getExtraContents()) {
            if (armor != null) {
                points += weights.getOrDefault(armor.getType(), 0D);
            }
        }
        return points;
    }

    @Override
    public void closing() {}
}
