package com.extrahardmode.module;

import com.extrahardmode.ExtraHardMode;
import com.extrahardmode.config.RootConfig;
import com.extrahardmode.config.RootNode;
import com.extrahardmode.service.EHMModule;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

import java.util.HashMap;
import java.util.UUID;

public class ScoreboardStatusManager extends EHMModule {

    private ScoreboardManager scoreboardManager;

    private final HashMap<UUID, Popup> popups = new HashMap<>();

    private static class Popup {

        public final long timestamp;

        public final Scoreboard scoreboard;

        public final Objective objective;

        public Popup(ScoreboardManager scoreboardManager, long mTimestamp, String title) {
            this.timestamp = mTimestamp;
            this.scoreboard = scoreboardManager.getNewScoreboard();
            this.objective = scoreboard.registerNewObjective("weight_board", "dummy", "weight_board");
            this.objective.setDisplayName(title);
            this.objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        }
    }

    public ScoreboardStatusManager(ExtraHardMode plugin) {
        super(plugin);
    }

    public void starting() {
        this.scoreboardManager = Bukkit.getScoreboardManager();

        RootConfig CFG = plugin.getModuleForClass(RootConfig.class);
        final int popupLifetimeTicks = CFG.getInt(RootNode.POPUP_LIFETIME_MS);

        plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, () -> {
            for (Player player : plugin.getServer().getOnlinePlayers()) {
                UUID uuid = player.getUniqueId();
                if (popups.containsKey(uuid)) {
                    if ((System.currentTimeMillis() - popups.get(uuid).timestamp) > popupLifetimeTicks) {
                        hidePopup(player);
                        popups.remove(uuid);
                    }
                }
            }
        }, 20*4, 30);
    }

    public void showPopup(Player player, String title, String line) {
        if (player != null && player.isOnline()) {
            Popup popup = new Popup(scoreboardManager, System.currentTimeMillis(), title);
            Score first = popup.objective.getScore(line);
            first.setScore(0);

            player.setScoreboard(popup.scoreboard);
            popups.put(player.getUniqueId(), popup);
        }
    }

    public void hidePopup(Player player) {
        player.setScoreboard(scoreboardManager.getMainScoreboard());
    }

    public void closing() {}
}
