package com.extrahardmode.module;

import com.extrahardmode.ExtraHardMode;
import com.extrahardmode.service.EHMModule;
import org.bukkit.Material;
import org.bukkit.Tag;

public class BlockModule extends EHMModule {

    public BlockModule(ExtraHardMode plugin) {
        super(plugin);
    }

    @Override
    public void starting() {}


    /**
     * Get the Material that will be dropped if this Block is broken by a player e.g. stone -> cobblestone ice -> nothing Note: This method doesn't have all blocks and is only
     * meant for blocks that you dont want to drop like grass/ice blocks
     *
     * @param mat to get the drop for
     */
    public static Material getDroppedMaterial(Material mat) {
        if (Tag.LEAVES.isTagged(mat))
            return Material.AIR;

        switch (mat) {
            case GRASS_BLOCK:
            case FARMLAND:
                return Material.DIRT;
            case STONE:
                return Material.COBBLESTONE;
            case COAL_ORE:
                return Material.COAL;
            case LAPIS_ORE:
                return Material.INK_SAC;
            case EMERALD_ORE:
                return Material.EMERALD;
            case REDSTONE_ORE:
                return Material.REDSTONE;
            case DIAMOND_ORE:
                return Material.DIAMOND;
            case NETHER_QUARTZ_ORE:
                return Material.QUARTZ;
            case ICE:
            case SPAWNER:
                return Material.AIR;
        }
        return mat;
    }

    @Override
    public void closing() {}
}
