package com.extrahardmode.module;

import com.extrahardmode.ExtraHardMode;
import com.extrahardmode.service.EHMModule;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.AbstractMap.SimpleEntry;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


/** Manages miscellaneous data. */
public class DataStoreModule extends EHMModule {

    /** List of previous locations. */
    private final List<SimpleEntry<Player, Location>> previousLocations = new CopyOnWriteArrayList<>();


    public DataStoreModule(ExtraHardMode plugin) {
        super(plugin);
    }


    @Override
    public void starting() {}


    @Override
    public void closing() {
        previousLocations.clear();
    }


    /**
     * Get the list of previous locations of players.
     *
     * @return List of players to location entries.
     */
    public List<SimpleEntry<Player, Location>> getPreviousLocations() {
        return previousLocations;
    }

}
