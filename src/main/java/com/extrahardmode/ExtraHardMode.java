package com.extrahardmode;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.extrahardmode.config.RootConfig;
import com.extrahardmode.enchantments.CustomEnchants;
import com.extrahardmode.enchantments.EnchantListener;
import com.extrahardmode.features.*;
import com.extrahardmode.features.monsters.*;
import com.extrahardmode.module.*;
import com.extrahardmode.service.IModule;
import com.extrahardmode.service.OurRandom;
import com.extrahardmode.task.MoreMonstersTask;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

public class ExtraHardMode extends JavaPlugin {

    private final Map<Class<? extends IModule>, IModule> modules = new LinkedHashMap<>();
    private final Random randomNumberGenerator = new Random();
    public static ProtocolManager protocolManager;

    public static final String staminaKey = "stamina";

    @Override
    public void onEnable() {

        protocolManager = ProtocolLibrary.getProtocolManager();

        protocolManager.addPacketListener(
                new PacketAdapter(this, ListenerPriority.NORMAL, PacketType.Play.Server.EXPERIENCE) {
                    @Override
                    public void onPacketSending(PacketEvent event) {
                        if (!event.getPacket().getMeta(staminaKey).isPresent()) {
                            event.setCancelled(true);
                        }
                    }
                });

        // Register modules
        registerModule(RootConfig.class, new RootConfig(this));

        registerModule(ScoreboardStatusManager.class, new ScoreboardStatusManager(this));

        registerModule(DataStoreModule.class, new DataStoreModule(this));
        registerModule(BlockModule.class, new BlockModule(this));
        registerModule(UtilityModule.class, new UtilityModule(this));
        registerModule(PlayerModule.class, new PlayerModule(this));

        //Basic Modules
        registerModule(AnimalCrowdControl.class, new AnimalCrowdControl(this));
        registerModule(Explosions.class, new Explosions(this));
        registerModule(Players.class, new Players(this));
        registerModule(Movement.class, new Movement(this));

        //Monster Modules
        registerModule(Blazes.class, new Blazes(this));
        registerModule(BumBumBens.class, new BumBumBens(this));
        registerModule(Endermen.class, new Endermen(this));
        registerModule(Ghasts.class, new Ghasts(this));
        registerModule(Horses.class, new Horses(this));

        registerModule(MonsterRules.class, new MonsterRules(this));
        registerModule(PigMen.class, new PigMen(this));
        registerModule(Skeletons.class, new Skeletons(this));
        registerModule(Spiders.class, new Spiders(this));
        registerModule(Zombies.class, new Zombies(this));
        registerModule(Witches.class, new Witches(this));
        registerModule(Vindicator.class, new Vindicator(this));
        registerModule(CaveSpider.class, new CaveSpider(this));
        registerModule(Guardians.class, new Guardians(this));
        registerModule(Vex.class, new Vex(this));

        OurRandom.reload();

        // FEATURE: monsters spawn in the light under a configurable Y level
        MoreMonstersTask task = new MoreMonstersTask(this);
        this.getServer().getScheduler().scheduleSyncRepeatingTask(this, task, 600L, 600L);

        // Custom enchantments
        CustomEnchants.register();
        registerModule(EnchantListener.class, new EnchantListener(this));
    }

    @Override
    public void onDisable() {
        super.onDisable();
        for (IModule module : modules.values()) { // Gracefully stop all modules
            module.closing();
        }
        for (Player player : getServer().getOnlinePlayers()) {
            player.setWalkSpeed(0.2F);
        }
        this.getServer().getScheduler().cancelTasks(this);
        modules.clear();
    }


    /**
     * Computes random chance
     *
     * @param percentChance - Percentage of success.
     *
     * @return True if it was successful, else false.
     */
    public boolean random(int percentChance) {
        return randomNumberGenerator.nextInt(101) < percentChance;
    }


    /**
     * Get random generator.
     *
     * @return a Random object
     */
    public Random getRandom() {
        return randomNumberGenerator;
    }


    /**
     * Register a module.
     *
     * @param clazz  - Class of the instance.
     * @param module - Module instance.
     *
     * @throws IllegalArgumentException - Thrown if an argument is null.
     */
    <T extends IModule> void registerModule(Class<T> clazz, T module) {
        if (clazz == null) {
            throw new IllegalArgumentException("Class cannot be null");
        } else if (module == null) {
            throw new IllegalArgumentException("Module cannot be null");
        }
        modules.put(clazz, module);
        module.starting();
    }

    /**
     * Retrieve a registered module.
     *
     * @param clazz - Class identifier.
     *
     * @return Module instance. Returns null is an instance of the given class has not been registered with the API.
     */
    public <T extends IModule> T getModuleForClass(Class<T> clazz) {
        return clazz.cast(modules.get(clazz));
    }

}
