package com.extrahardmode.config;

import com.extrahardmode.service.IoHelper;
import com.extrahardmode.service.config.*;
import com.extrahardmode.service.config.customtypes.BlockRelationsList;
import com.extrahardmode.service.config.customtypes.PotionEffectHolder;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * A Wrapper that contains <ul> <li>a FileConfiguration</li> <li>A reference to the config file</li> <li>Information
 * about the Mode this Config is loaded in</li> <li>Information about which status this Config is in</li> <li></li></ul>
 */
public class EHMConfig {
    /**
     * Nodes to load from the config
     */
    private final Set<ConfigNode> mConfigNodes = new LinkedHashSet<>();

    /**
     * Loaded config values
     */
    private final Map<ConfigNode, Object> mLoadedNodes = new HashMap<>();

    /**
     * Location we loaded the File from
     */
    private final File mConfigFile;

    /**
     * Loaded FileConfiguration
     */
    private final FileConfiguration mConfig;

    /**
     * Header is at the top of the file
     */
    private Header mHeader;

    /**
     * Mode with which this config will get loaded
     */
    private Mode mMode = Mode.NOT_SET;

    /**
     * Worlds in which this config is active in
     */
    private final Set<String> mWorlds = new LinkedHashSet<>(); // Linked: keeps inserted order

    /**
     * If the header on top the config should be printed
     */
    private boolean mPrintHeader = true;

    /**
     * If line comments should be printed
     */
    private boolean mPrintComments = true;

    /**
     * Node that holds the mode of the config (string)
     */
    private final ConfigNode mModeNode = RootNode.MODE;

    /**
     * Node that determines if the header should be printed
     */
    private final ConfigNode mPrintHeaderNode = RootNode.PRINT_HEADER;

    /**
     * Should node comments be printed
     */
    private final ConfigNode mPrintCommentsNode = RootNode.PRINT_COMMENTS;


    /**
     * Constructor
     *
     * @param file configuration file
     */
    public EHMConfig(File file) {
        this.mConfigFile = file;
        this.mConfig = YamlConfiguration.loadConfiguration(mConfigFile);
    }


    /**
     * Initializes and fully loads this configuration
     * <pre>
     * - load mode
     * - load worlds
     * - load nodes
     * - verify nodes
     * </pre>
     */
    public void load() {
        if (mConfigNodes.isEmpty()) {
            throw new IllegalStateException("You forgot to add nodes to " + mConfigFile.getName());
        }
        loadMode();
        loadCommentOptions();
        loadNodes();
        validateNodes();
        loadWorlds();
    }


    /**
     * Saves the config to file
     */
    public void save() {
        if (mLoadedNodes.isEmpty())
            throw new IllegalStateException("No nodes are loaded, nothing to save to " + mConfigFile.getName());
        saveNodes();
        if (mPrintHeader)
            writeHeader();
    }


    public Map<ConfigNode, Object> getLoadedNodes()
    {
        return mLoadedNodes;
    }


    /**
     * Returns the File to save the FileConfiguration to
     */
    public File getConfigFile() {
        return mConfigFile;
    }


    /**
     * Get the Mode this config should be loaded
     *
     * @return mode or Mode.NOT_SET if not set yet
     */
    public Mode getMode() {
        return mMode;
    }


    /**
     * Set the Mode with which this Config should be loaded
     *
     * @param mode to set
     */
    public void setMode(Mode mode)
    {
        this.mMode = mode;
    }


    /**
     * Register new nodes to load from the config
     *
     * @param nodes nodes to register
     */
    public void registerNodes(ConfigNode[] nodes)
    {
        Collections.addAll(mConfigNodes, nodes);
    }


    /**
     * Get the worlds for this config
     *
     * @return worlds in which the config is active
     */
    public Collection<String> getWorlds()
    {
        return mWorlds;
    }


    /**
     * Load the mode from the config
     */
    public void loadMode() {
        //DETERMINE MODE FIRST
        String modeString = mConfig.getString(mModeNode.getPath());
        try {
            if (modeString != null) {
                setMode(Mode.valueOf(modeString.toUpperCase()));
            }
        } catch (IllegalArgumentException ignored) {}
        finally {
            if (getMode() == null || getMode() == Mode.NOT_SET) {
                if (isMainConfig()) {
                    setMode(Mode.MAIN);
                } else {
                    setMode(Mode.INHERIT);
                }
            }
        }
    }


    /**
     * Load the worlds where this config is active
     */
    public void loadWorlds() {
        mWorlds.addAll((List<String>) mLoadedNodes.get(RootNode.WORLDS));
    }


    public void loadCommentOptions() {
        mPrintHeader = mConfig.getBoolean(mPrintHeaderNode.getPath(), true);
        mPrintComments = mConfig.getBoolean(mPrintCommentsNode.getPath(), true);
    }


    /**
     * Load all values from the config and save in our map
     */
    public void loadNodes() {
        for (ConfigNode node : mConfigNodes) {
            Object obj = null;

            switch (node.getVarType()) {
                case LIST: {
                    if (mConfig.get(node.getPath()) instanceof List)
                        obj = mConfig.getStringList(node.getPath());
                    break;
                }
                case DOUBLE: {
                    if (mConfig.get(node.getPath()) instanceof Double)
                        obj = mConfig.getDouble(node.getPath());
                    break;
                }
                case STRING: {
                    if (mConfig.get(node.getPath()) instanceof String)
                        obj = mConfig.getString(node.getPath());
                    break;
                }
                case INTEGER: {
                    if (mConfig.get(node.getPath()) instanceof Integer)
                        obj = mConfig.getInt(node.getPath());
                    break;
                }
                case BOOLEAN: {
                    if (mConfig.get(node.getPath()) instanceof Boolean)
                        obj = mConfig.getBoolean(node.getPath());
                    break;
                }
                case POTION_EFFECT: {
                    ConfigurationSection section = mConfig.getConfigurationSection(node.getPath());
                    obj = PotionEffectHolder.loadFromConfig(section);
                    break;
                }
                case MATERIAL: {
                    if (mConfig.getString(node.getPath()) != null)
                        obj = Material.matchMaterial(Objects.requireNonNull(mConfig.getString(node.getPath())));
                    break;
                }
                case BLOCK_RELATION_LIST: {
                    if (mConfig.get(node.getPath()) instanceof List) {
                        List<String> list = mConfig.getStringList(node.getPath());
                        BlockRelationsList blocks = new BlockRelationsList();
                        for (String str : list)
                            blocks.addFromConfig(str);
                        obj = blocks;
                    } else if (mConfig.isSet(node.getPath()))
                        obj = BlockRelationsList.EMPTY_LIST;
                    break;
                }
                //ignore comments
                case COMMENT:
                    continue;
                default: {
                    throw new UnsupportedOperationException(node.getPath() + "No specific getter available for Type: " + " " + node.getVarType());
                }
            }
            mLoadedNodes.put(node, obj);
        }
    }


    /**
     * Make sure that all our loaded values are valid and usable by the plugin
     */
    public void validateNodes() {
        for (ConfigNode node : mConfigNodes) {
            switch (node.getVarType()) {
                case INTEGER: {
                    Integer validated = Validation.validateInt(node, mLoadedNodes.get(node));
                    mLoadedNodes.put(node, validated);
                    break;
                }
                case COMMENT:
                    break;
                //TODO ADD BLOCKTYPE_LIST
                default: {
                    Object validateMe = mLoadedNodes.get(node);
                    if (validateMe == null)
                        mLoadedNodes.put(node, node.getDefaultValue());
                }
            }
            //Make sure the string of the node matches
            if (node == mModeNode)
                mLoadedNodes.put(node, mMode.name());
        }
    }


    /**
     * Writes all our validated objects back to the config in the order they were added
     */
    private void saveNodes() {
        FileConfiguration outConfig = new YamlConfiguration();
        for (ConfigNode node : mConfigNodes) {
            Object value = mLoadedNodes.get(node);
            //Custom writing code for our custom objects
            switch (node.getVarType()) {
                case MATERIAL: {
                    if (value instanceof Material) {
                        outConfig.set(node.getPath(), ((Material)value).name());
                        break;
                    }
                }
                case BLOCK_RELATION_LIST: {
                    if (value instanceof BlockRelationsList) {
                        String[] blockStrings = ((BlockRelationsList) value).toConfigStrings();
                        outConfig.set(node.getPath(), blockStrings);
                        break;
                    }
                }
                case POTION_EFFECT: {
                    if (value instanceof PotionEffectHolder)
                    {
                        ((PotionEffectHolder) value).saveToConfig(outConfig, node.getPath());
                        break;
                    }
                }
                case COMMENT:
                    break;
                default: {
                    outConfig.set(node.getPath(), value);
                }
            }
        }
        try {
            outConfig.save(mConfigFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeHeader() {
        if (mHeader == null)
            return;
        ByteArrayOutputStream memHeaderStream = null;
        OutputStreamWriter memWriter = null;
        try {
            //Write Header to a temporary file
            memHeaderStream = new ByteArrayOutputStream();
            memWriter = new OutputStreamWriter(memHeaderStream, StandardCharsets.UTF_8.newEncoder());
            memWriter.write(mHeader.toString());
            memWriter.close();
            //Copy Header to the beginning of the config file
            IoHelper.writeHeader(mConfigFile, memHeaderStream);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (memHeaderStream != null) memHeaderStream.close();
                if (memWriter != null) memWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public boolean isMainConfig()
    {
        return mConfigFile.getName().equals("config.yml");
    }


    public void setHeader(Header header)
    {
        this.mHeader = header;
    }


    public boolean printComments()
    {
        return mPrintComments;
    }
}
