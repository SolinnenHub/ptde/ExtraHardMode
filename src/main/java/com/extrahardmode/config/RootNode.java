package com.extrahardmode.config;

import com.extrahardmode.service.config.ConfigNode;
import com.extrahardmode.service.config.MultiWorldConfig;
import com.extrahardmode.service.config.customtypes.BlockRelationsList;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Configuration options of the root config.yml file.
 */
// Please keep the code style, it makes it easier to grasp the structure of the config
@SuppressWarnings("SameParameterValue")
public enum RootNode implements ConfigNode {
    /**
     * How this ConfigFile is going to be handled by the plugin
     */
    MODE("Config Type", VarType.STRING, "MAIN"),
    /**
     * Print Header?
     */
    PRINT_HEADER("Print Config Header", VarType.BOOLEAN, true, "If the big text on top of the config should be printed"),
    /**
     * Print Node Comments?
     */
    PRINT_COMMENTS("Print Comments", VarType.BOOLEAN, true, "If comments like this should be printed"),
    /**
     * list of worlds where extra hard mode rules apply
     */
    WORLDS("Enabled Worlds", VarType.LIST, new DefaultWorlds(),
            "Set the worlds you want ehm active here. E.g. [world, world_nether]. \"@all\" enables ehm for all worlds"),


    /*
     * #################
     * # WORLD EFFECTS #
     * #################
     */
    /**
     * whether players catch fire when extinguishing a fire up close
     */
    EXTINGUISHING_FIRE_IGNITES_PLAYERS("Player.Extinguishing Fires Ignites Player", VarType.BOOLEAN, true,
            "Set the player on fire when he tries to extinguish fire with his bare hand."),
    /**
     * how much health after respawn
     */
    PLAYER_RESPAWN_HEALTH_PERCENTAGE("Player.Death.Override Respawn Health.Percentage", VarType.INTEGER, SubType.PERCENTAGE, Disable.HUNDRED, 75,
            "Percentage of total health that the player will spawn with. Works with custom max health."),
    /**
     * how much health after respawn
     */
    EXTINGUISHING_FIRE_IGNITES_PLAYERS_PERCENTAGE("Player.Extinguishing Fires Ignites Player Percentage", VarType.INTEGER, SubType.PERCENTAGE, Disable.HUNDRED, 30,
            "Percentage of player ignition by fire during attempt to put it out."),
    /**
     * how much food bar after respawn
     */
    PLAYER_RESPAWN_FOOD_LEVEL("Player.Death.Respawn Foodlevel", VarType.INTEGER, SubType.HEALTH, 15,
            "How many food hunches a player will spawn with"),

    /*
     * #################
     * # ARMOR CHANGES #
     * #################
     */
    /**
     * Speed of player walking with no worn armor
     */
    ARMOR_SLOWDOWN_BASESPEED("Player.Armor Changes.Basespeed", VarType.DOUBLE, 0.20,
            "Player speed with no armor. Minecraft default is 0.2.",
            "Slightly increase to 0.21 to give players with no armor an advantage."),
    /**
     * Maximum percentage
     */
    ARMOR_SLOWDOWN_PERCENT("Player.Armor Changes.Slowdown Percentage", VarType.INTEGER, SubType.PERCENTAGE, 33,
            "How much percent players wearing a full diamond armor will be slowed down.",
            "This is the maximum slow down, the amount of armor points determines how much a player will be slowed down."),

    /*
     * #########################
     * # GENERAL MONSTER RULES #
     * #########################
     */
    /**
     * max y value for extra monster spawns
     */
    MORE_MONSTERS_MAX_Y("General Monster Rules.More Monsters.Max Y", VarType.INTEGER, SubType.Y_VALUE, Disable.ZERO, 55),
    /**
     * what to multiply monster spawns by
     */
    MORE_MONSTERS_MULTIPLIER("General Monster Rules.More Monsters.Multiplier", VarType.INTEGER, SubType.NATURAL_NUMBER, Disable.ONE, 2,
            "A simple multiplier to increase spawns under ground by increasing the packspawning size."),

    /**
     * max y value for monsters to spawn in the light
     */
    MONSTER_SPAWNS_IN_LIGHT_MAX_Y("General Monster Rules.Monsters Spawn In Light.Max Y", VarType.INTEGER, SubType.Y_VALUE, Disable.ZERO, 50),

    /**
     * max light value for monsters to spawn in the light
     * 0-3  = bats spawn at depth
     * 0-7  = mobs spawn in overworld normally
     * 8-11 = mobs are hostile but do not burn
     * 12+  = mobs except spiders, creepers, and witches burn (blazes hostile)
     */
    MONSTER_SPAWNS_IN_LIGHT_MAX_LIGHT("General Monster Rules.Monsters Spawn In Light.Max Light", VarType.INTEGER, SubType.NATURAL_NUMBER, Disable.ZERO, 10,
            "0-3: bats spawning, 0-7 normal mob spawning, 8-11 mobs are hostile but don't burn, 12+ mobs burn"),

    /**
     * percentage of time to spawn monsters in light
     */
    MONSTER_SPAWNS_IN_LIGHT_PERCENTAGE("General Monster Rules.Monsters Spawn In Light.Percentage", VarType.INTEGER, SubType.PERCENTAGE, Disable.ZERO, 100,
            "Spawns monsters at locations where player has been previously."),


    /*
     * #############
     * # SKELETONS #
     * #############
     */
    /**
     * Enable Snowball Arrows
     */
    SKELETONS_SNOWBALLS_ENABLE("Skeletons.Shoot Snowballs.Enable", VarType.BOOLEAN, true),
    /**
     * How often should a snowball be shot
     */
    SKELETONS_SNOWBALLS_PERCENT("Skeletons.Shoot Snowballs.Percent", VarType.INTEGER, SubType.PERCENTAGE, 20),
    /**
     * Slowness length
     */
    SKELETONS_SNOWBALLS_SLOW_LEN("Skeletons.Shoot Snowballs.Blind Player (ticks)", VarType.INTEGER, SubType.NATURAL_NUMBER, 100),

    /**
     * Shoot Fireworks
     */
    SKELETONS_FIREWORK_ENABLE("Skeletons.Shoot Fireworks.Enable", VarType.BOOLEAN, true),
    /**
     * Knockback Players?
     */
    SKELETONS_FIREWORK_PERCENT("Skeletons.Shoot Fireworks.Percent", VarType.INTEGER, SubType.PERCENTAGE, 30),
    /**
     * Knockback Player strength, multiplier
     */
    SKELETONS_FIREWORK_KNOCKBACK_VEL("Skeletons.Shoot Fireworks.Knockback Player Velocity", VarType.DOUBLE, 1.0D),

    /**
     * Skeletons can shoot fireballs whcih set you on fire
     */
    SKELETONS_FIREBALL_ENABLE("Skeletons.Shoot Fireballs.Enable", VarType.BOOLEAN, true),


    SKELETONS_FIREBALL_PERCENTAGE("Skeletons.Shoot Fireballs.Percentage", VarType.INTEGER, SubType.PERCENTAGE, 10),


    SKELETONS_FIREBALL_PLAYER_FIRETICKS("Skeletons.Shoot Fireballs.Player Fireticks", VarType.INTEGER, SubType.NATURAL_NUMBER, 40),

    /**
     * enable skeletons shooting silverfish instead of firing arrows
     */
    SKELETONS_RELEASE_SILVERFISH_ENABLE("Skeletons.Shoot Silverfish.Enable", VarType.BOOLEAN, true),
    /**
     * percent chance skeletons will release silverfish instead of firing arrows
     */
    SKELETONS_RELEASE_SILVERFISH_PERCENT("Skeletons.Shoot Silverfish.Percent", VarType.INTEGER, SubType.PERCENTAGE, 20),
    /**
     * percent chance skeletons will release silverfish instead of firing arrows
     */
    SKELETONS_RELEASE_SILVERFISH_LIMIT("Skeletons.Shoot Silverfish.Limit To X Spawned At A Time", VarType.INTEGER, SubType.NATURAL_NUMBER, 5),
    /**
     * total limit of silverfish
     */
    SKELETONS_RELEASE_SILVERFISH_LIMIT_TOTAL("Skeletons.Shoot Silverfish.Limit To X Spawned In Total", VarType.INTEGER, SubType.NATURAL_NUMBER, 15),
    /**
     * whether or not arrows will pass harmlessly through skeletons
     */
    SKELETONS_DEFLECT_ARROWS("Skeletons.Deflect Arrows Percent", VarType.INTEGER, SubType.PERCENTAGE, 100),

    /*
     * ###########
     * # SPIDERS #
     * ###########
     */
    /**
     * percentage of zombies which will be replaced with spiders under sea level
     */
    BONUS_UNDERGROUND_SPIDER_SPAWN_PERCENT("Spiders.Bonus Underground Spawn Percent", VarType.INTEGER, SubType.PERCENTAGE, 20),
    /**
     * whether spiders drop webbing when they die
     */
    SPIDERS_DROP_WEB_ON_DEATH("Spiders.Drop Web On Death", VarType.BOOLEAN, true),

    /*
     * ############
     * # CREEPERS #
     * ############
     */
    /**
     * percentage of creepers which will spawn charged
     */
    CHARGED_CREEPER_SPAWN_PERCENT("Creepers.Charged Creeper Spawn Percent", VarType.INTEGER, SubType.PERCENTAGE, 10),
    /**
     * whether creepers explode when caught on fire
     */
    FLAMING_CREEPERS_EXPLODE("Creepers.Fire Triggers Explosion.Enable", VarType.BOOLEAN, true),
    /**
     * Number of Fireworks to show when creeper launches
     */
    FLAMING_CREEPERS_FIREWORK("Creepers.Fire Triggers Explosion.Firework Count", VarType.INTEGER, SubType.NATURAL_NUMBER, 3),
    /**
     * Speed at which a creeper ascends
     */
    FLAMING_CREEPERS_ROCKET("Creepers.Fire Triggers Explosion.Launch In Air Speed", VarType.DOUBLE, 0.5),

    /*
     * ##########
     * # BLAZES #
     * ##########
     */
    /**
     * percentage of skeletons near bedrock which will be replaced with blazes
     */
    NEAR_BEDROCK_BLAZE_SPAWN_PERCENT("Blazes.Near Bedrock Spawn Percent", VarType.INTEGER, SubType.PERCENTAGE, 50),
    /**
     * percentage of pig zombies which will be replaced with blazes
     */
    BONUS_NETHER_BLAZE_SPAWN_PERCENT("Blazes.Bonus Nether Spawn Percent", VarType.INTEGER, SubType.PERCENTAGE, 20),


    /*
     * ##############
     * # PIGZOMBIES #
     * ##############
     */
    /**
     * Whether pig zombies should drop netherwart occasionally elsewhere in Nether
     */
    NETHER_PIGS_DROP_WART("PigZombies.Percent Chance to Drop Netherwart", VarType.INTEGER, SubType.PERCENTAGE, 3),

    /*
     * ##########
     * # GHASTS #
     * ##########
     */
    /**
     * whether ghasts should deflect arrows and drop extra loot percentage like skeleton deflect
     */
    GHASTS_DEFLECT_ARROWS("Ghasts.Arrows Do % Damage", VarType.INTEGER, SubType.PERCENTAGE, Disable.HUNDRED, 20,
            "Reduce the damage arrows do to Ghasts to make fights with Ghasts more challenging."),
    /**
     * whether ghasts should deflect arrows and drop extra loot percentage like skeleton deflect
     */
    GHASTS_EXP_MULTIPLIER("Ghasts.Exp Multiplier", VarType.INTEGER, SubType.NATURAL_NUMBER, Disable.ONE, 10),
    /**
     * whether ghasts should deflect arrows and drop extra loot percentage like skeleton deflect
     */
    GHASTS_DROPS_MULTIPLIER("Ghasts.Drops Multiplier", VarType.INTEGER, SubType.NATURAL_NUMBER, Disable.ONE, 2),

    /*
     * ##########
     * # POPUPS #
     * ##########
     */
    /**
     * Should Stone be turned to cobblestone
     */
    POPUP_LIFETIME_MS("PopUps.Lifetime Ms", VarType.INTEGER, 1000*5,
            "Lifetime of popup in milliseconds."),

    /*
     * ###########
     * # WITCHES #
     * ###########
     */
    /**
     * Do Witches have additional attacks
     */
    WITCHES_ADDITIONAL_ATTACKS("Witches.Additional Attacks", VarType.BOOLEAN, true,
            "Includes spawning of baby zombies, explosions and teleporting"),
    /**
     * percentage of surface zombies which spawn as witches
     */
    BONUS_WITCH_SPAWN_PERCENT("Witches.Bonus Spawn Percent", VarType.INTEGER, SubType.PERCENTAGE, 5),


    /*
     * ##############
     * # Vindicator #
     * ##############
     */
    /**
     * percentage of skeletons which spawn as vindicators in roofed forests.
     */
    BONUS_VINDICATOR_SPAWN_PERCENT("Vindicator.Bonus Spawn Percent", VarType.INTEGER, SubType.PERCENTAGE, 20),

    /*
     * #############
     * # Guardians #
     * #############
     */
    /**
     * percentage of squids which spawn as Guardians in ocean and deep ocean.
     */
    BONUS_GUARDIANS_SPAWN_PERCENT("Guardians.Bonus Spawn Percent", VarType.INTEGER, SubType.PERCENTAGE, 20),

    /*
     * #######
     * # VEX #
     * #######
     */
    /**
     * percentage of bats which spawn as Vex.
     */
    BONUS_VEX_SPAWN_PERCENT("Vex.Bonus Spawn Percent", VarType.INTEGER, SubType.PERCENTAGE, 2),

    /*
     * ###########
     * # FARMING #
     * ###########
     */
    /**
     * Prevent animal overcrowding on a small area
     */
    ANIMAL_OVERCROWD_CONTROL("Farming.Animal Overcrowding Control.Enable", VarType.BOOLEAN, true),
    /**
     * Threshold/Number of animals before start damaging animals
     */
    ANIMAL_OVERCROWD_THRESHOLD("Farming.Animal Overcrowding Control.Threshold", VarType.INTEGER, SubType.NATURAL_NUMBER, 10, 
            "Maximum amount of animals allowed in a small area before they start dying"),

    /*
     * #############################
     * # ADDITIONAL FALLING BLOCKS #
     * #############################
     */
    /**
     * Whether a falling block that is broken by an obstructing block should drop as an item
     */
    MORE_FALLING_BLOCKS_DROP_ITEM("Additional Falling Blocks.Drop As Items", VarType.BOOLEAN, false,
            "Whether a falling block that is broken by an obstructing block should drop as an item"),


    /*
     * #####################
     * # EXPLOSION PHYSICS #
     * #####################
     */
    /**
     * Enable cool flying blocks
     */
    EXPLOSIONS_FYLING_BLOCKS_ENABLE("Explosions.Physics.Enable", VarType.BOOLEAN, true,
            "Makes explosions uber cool by throwing blocks up into the air"),
    /**
     * If explosions from other plugins should also be affected (disabled by default)
     */
    EXPLOSIONS_FYLING_BLOCKS_ENABLE_OTHER("Explosions.Physics.Enable For Plugin Created Explosions", VarType.BOOLEAN, false),
    /**
     * How many blocks will go flying
     */
    EXPLOSIONS_FLYING_BLOCKS_PERCENTAGE("Explosions.Physics.Blocks Affected Percentage", VarType.INTEGER, SubType.PERCENTAGE, 20,
            "How many of the blocks that would have been destroyed should go flying instead"),
    /**
     * How fast the blocks accelerate upwards
     */
    EXPLOSIONS_FLYING_BLOCKS_UP_VEL("Explosions.Physics.Up Velocity", VarType.DOUBLE, 2.0,
            "Following 2 variables basically determine the angle and speed in what the blocks go flying"),
    /**
     * How far the blocks spread
     */
    EXPLOSIONS_FLYING_BLOCKS_SPREAD_VEL("Explosions.Physics.Spread Velocity", VarType.DOUBLE, 3.0),
    /**
     * In what radius the flying blocks shouldnt be placed
     */
    EXPLOSIONS_FLYING_BLOCKS_AUTOREMOVE_RADIUS("Explosions.Physics.Exceed Radius Autoremove", VarType.INTEGER, SubType.NATURAL_NUMBER, 10,
            "Blocks exceeding this radius will no be placed in the world to avoid explosions uglying the landscape.",
            "Set to 0 if you want blocks to not be placed at all"),
    /**
     * This determines if the explosion is categorized as under or above
     */
    EXPLOSIONS_Y("Explosions.Border Y", VarType.INTEGER, SubType.NATURAL_NUMBER, 55,
            "Determines where your surface is located. You can have seperate settings for the surface and caves."),

    //WHEN ADDING NEW EXPLOSIONTYPES YOU HAVE TO ADD THE NODES TO EXPLOSIONTYPE AND ALSO UPDATE THE EXPLOSIONTASK
    /**
     * Size of Explosion below border
     */
    EXPLOSIONS_CREEPERS_BELOW_POWER("Explosions.Creeper.Below Border.Explosion Power", VarType.INTEGER, SubType.NATURAL_NUMBER, 3,
            "3 = default creeper, 4 = default tnt, 6 = default charged creeper"),
    /**
     * Set Fire on Explosion below border
     */
    EXPLOSIONS_CREEPERS_BELOW_FIRE("Explosions.Creeper.Below Border.Set Fire", VarType.BOOLEAN, false),
    /**
     * Damage the world below border
     */
    EXPLOSIONS_CREEPERS_BELOW_WORLD_GRIEF("Explosions.Creeper.Below Border.World Damage", VarType.BOOLEAN, true),
    /**
     * Size of Explosion below border
     */
    EXPLOSIONS_CREEPERS_ABOVE_POWER("Explosions.Creeper.Above Border.Explosion Power", VarType.INTEGER, SubType.NATURAL_NUMBER, 3),
    /**
     * Set Fire on Explosion below border
     */
    EXPLOSIONS_CREEPERS_ABOVE_FIRE("Explosions.Creeper.Above Border.Set Fire", VarType.BOOLEAN, false),
    /**
     * Damage the world below border
     */
    EXPLOSIONS_CREEPERS_ABOVE_WORLD_GRIEF("Explosions.Creeper.Above Border.World Damage", VarType.BOOLEAN, true,
            "Disabling worlddamage allows you to have explosions that damage players above ground, but doesn't make a mess."),


    /**
     * Size of Explosion below the border
     */
    EXPLOSIONS_CHARGED_CREEPERS_BELOW_POWER("Explosions.Charged Creeper.Below Border.Explosion Power", VarType.INTEGER, SubType.NATURAL_NUMBER, 4),
    /**
     * Set Fire on Explosion below border
     */
    EXPLOSIONS_CHARGED_CREEPERS_BELOW_FIRE("Explosions.Charged Creeper.Below Border.Set Fire", VarType.BOOLEAN, false),
    /**
     * Damage the world below border
     */
    EXPLOSIONS_CHARGED_CREEPERS_BELOW_WORLD_GRIEF("Explosions.Charged Creeper.Below Border.World Damage", VarType.BOOLEAN, true),
    /**
     * Size of Explosion below border
     */
    EXPLOSIONS_CHARGED_CREEPERS_ABOVE_POWER("Explosions.Charged Creeper.Above Border.Explosion Power", VarType.INTEGER, SubType.NATURAL_NUMBER, 4),
    /**
     * Set Fire on Explosion below border
     */
    EXPLOSIONS_CHARGED_CREEPERS_ABOVE_FIRE("Explosions.Charged Creeper.Above Border.Set Fire", VarType.BOOLEAN, false),
    /**
     * Damage the world below border
     */
    EXPLOSIONS_CHARGED_CREEPERS_ABOVE_WORLD_GRIEF("Explosions.Charged Creeper.Above Border.World Damage", VarType.BOOLEAN, true),

    /**
     * TNT Enable Custom Explosion?
     */
    EXPLOSIONS_TNT_ENABLE("Explosions.Tnt.Enable Custom Explosion", VarType.BOOLEAN, true),
    /**
     * whether TNT should explode multiple times
     */
    BETTER_TNT("Explosions.Tnt.Enable Multiple Explosions", VarType.BOOLEAN, true,
            "Creates 3 explosions at random locations close to the original tnt",
            "Makes for more natural looking craters."),
    /**
     * Size of Explosion below the border
     */
    EXPLOSIONS_TNT_BELOW_POWER("Explosions.Tnt.Below Border.Explosion Power", VarType.INTEGER, SubType.NATURAL_NUMBER, 5),
    /**
     * Set Fire on Explosion below border
     */
    EXPLOSIONS_TNT_BELOW_FIRE("Explosions.Tnt.Below Border.Set Fire", VarType.BOOLEAN, false),
    /**
     * Damage the world below border
     */
    EXPLOSIONS_TNT_BELOW_WORLD_GRIEF("Explosions.Tnt.Below Border.World Damage", VarType.BOOLEAN, true),
    /**
     * Size of Explosion below border
     */
    EXPLOSIONS_TNT_ABOVE_POWER("Explosions.Tnt.Above Border.Explosion Power", VarType.INTEGER, SubType.NATURAL_NUMBER, 3),
    /**
     * Set Fire on Explosion below border
     */
    EXPLOSIONS_TNT_ABOVE_FIRE("Explosions.Tnt.Above Border.Set Fire", VarType.BOOLEAN, false),
    /**
     * Damage the world below border
     */
    EXPLOSIONS_TNT_ABOVE_WORLD_GRIEF("Explosions.Tnt.Above Border.World Damage", VarType.BOOLEAN, true),


    /**
     * Size of Explosion below the border
     */
    EXPLOSIONS_BLAZE_BELOW_POWER("Explosions.Blazes Explode On Death.Below Border.Explosion Power", VarType.INTEGER, SubType.NATURAL_NUMBER, 4),
    /**
     * Set Fire on Explosion below border
     */
    EXPLOSIONS_BLAZE_BELOW_FIRE("Explosions.Blazes Explode On Death.Below Border.Set Fire", VarType.BOOLEAN, true),
    /**
     * Damage the world below border
     */
    EXPLOSIONS_BLAZE_BELOW_WORLD_GRIEF("Explosions.Blazes Explode On Death.Below Border.World Damage", VarType.BOOLEAN, true),
    /**
     * Size of Explosion below border
     */
    EXPLOSIONS_BLAZE_ABOVE_POWER("Explosions.Blazes Explode On Death.Above Border.Explosion Power", VarType.INTEGER, SubType.NATURAL_NUMBER, 4),
    /**
     * Set Fire on Explosion below border
     */
    EXPLOSIONS_BLAZE_ABOVE_FIRE("Explosions.Blazes Explode On Death.Above Border.Set Fire", VarType.BOOLEAN, true),
    /**
     * Damage the world below border
     */
    EXPLOSIONS_BLAZE_ABOVE_WORLD_GRIEF("Explosions.Blazes Explode On Death.Above Border.World Damage", VarType.BOOLEAN, true),

    /**
     * Ghast Enable custom Explosion?
     */
    EXPLOSIONS_GHASTS_ENABLE("Explosions.Ghasts.Enable Custom Explosion", VarType.BOOLEAN, true),
    /**
     * Size of Explosion below the border
     */
    EXPLOSIONS_GHAST_BELOW_POWER("Explosions.Ghasts.Below Border.Explosion Power", VarType.INTEGER, SubType.NATURAL_NUMBER, 2),
    /**
     * Set Fire on Explosion below border
     */
    EXPLOSIONS_GHAST_BELOW_FIRE("Explosions.Ghasts.Below Border.Set Fire", VarType.BOOLEAN, true),
    /**
     * Damage the world below border
     */
    EXPLOSIONS_GHAST_BELOW_WORLD_GRIEF("Explosions.Ghasts.Below Border.World Damage", VarType.BOOLEAN, true),
    /**
     * Size of Explosion below border
     */
    EXPLOSIONS_GHAST_ABOVE_POWER("Explosions.Ghasts.Above Border.Explosion Power", VarType.INTEGER, SubType.NATURAL_NUMBER, 2),
    /**
     * Set Fire on Explosion below border
     */
    EXPLOSIONS_GHAST_ABOVE_FIRE("Explosions.Ghasts.Above Border.Set Fire", VarType.BOOLEAN, true),
    /**
     * Damage the world below border
     */
    EXPLOSIONS_GHAST_ABOVE_WORLD_GRIEF("Explosions.Ghasts.Above Border.World Damage", VarType.BOOLEAN, true),;

    /**
     * Path.
     */
    private final String path;

    /**
     * Comment to be written to the file
     */
    private final String[] comments;

    /**
     * Variable type.
     */
    private final VarType type;

    /**
     * Subtype like percentage, y-value, health
     */
    private SubType subType = null;

    /**
     * Default value.
     */
    private final Object defaultValue;

    /**
     * The value that will disable this option
     */
    private Disable disableValue = null;


    /**
     * Constructor.
     *
     * @param path - Configuration path.
     * @param type - Variable type.
     * @param def  - Default value.
     */
    RootNode(String path, VarType type, Object def) {
        this.comments = null;
        this.path = path;
        this.type = type;
        this.defaultValue = def;
    }


    /**
     * Constructor.
     *
     * @param path     - Configuration path.
     * @param type     - Variable type.
     * @param def      - Default value.
     * @param comments - Explaining this node
     */
    RootNode(String path, VarType type, Object def, String... comments) {
        this.path = path;
        this.type = type;
        this.defaultValue = def;
        this.comments = comments;
    }


    RootNode(String path, VarType type, SubType subType, Object def) {
        this.comments = null;
        this.path = path;
        this.type = type;
        this.defaultValue = def;
        this.subType = subType;
    }


    RootNode(String path, VarType type, SubType subType, Object def, String... comments) {
        this.path = path;
        this.type = type;
        this.defaultValue = def;
        this.subType = subType;
        this.comments = comments;
    }


    RootNode(String path, VarType type, SubType subType, Disable disable, Object def) {
        this.comments = null;
        this.path = path;
        this.type = type;
        this.defaultValue = def;
        this.subType = subType;
        this.disableValue = disable;
    }


    RootNode(String path, VarType type, SubType subType, Disable disable, Object def, String... comments) {
        this.comments = comments;
        this.path = path;
        this.type = type;
        this.defaultValue = def;
        this.subType = subType;
        this.disableValue = disable;
    }


    public static final String baseNode = "ExtraHardMode";

    @Override
    public String getPath() {
        return baseNode + "." + path;
    }


    @Override
    public VarType getVarType()
    {
        return type;
    }


    @Override
    public Object getDefaultValue()
    {
        return defaultValue;
    }


    @Override
    public SubType getSubType()
    {
        return subType;
    }


    /**
     * Get comment describing this node
     *
     * @return comment or null if not set
     */
    public String[] getComments()
    {
        return comments;
    }


    /**
     * Get the Object that will disable this option
     * <pre>Defaults:
     * boolean: false
     * int:
     *   no SubType set: 0
     *   no Disable value: 0
     *   health: 20
     *   percentage: 0
     * double:
     *   same as int
     * string: ""
     *   subtype and disable ignored
     * list: .emptyList()
     * blocktypelist: same as list
     * </pre>
     *
     * @return Object that will disable this option in the plugin
     */
    @Override
    public Object getValueToDisable() {
        Object obj;
        switch (type) {
            case BOOLEAN: {
                obj = false;
                break;
            }
            case INTEGER: {
                obj = 0;
                if (subType != null) {
                    if (disableValue != null && disableValue.get() != null)
                        obj = disableValue.get();
                    else {
                        switch (subType) {
                            case NATURAL_NUMBER:
                            case Y_VALUE:
                            case PERCENTAGE: {
                                obj = 0;
                                break;
                            }
                            case HEALTH: {
                                obj = 20;
                                break;
                            }
                            default: {
                                throw new UnsupportedOperationException("SubType hasn't been specified for " + path);
                            }
                        }
                    }
                }
                break;
            }
            case DOUBLE: {
                obj = 0.0;
                if (subType != null)
                    if (disableValue != null && disableValue.get() != null)
                        obj = disableValue.get();
                    else {
                        switch (subType) {
                            case NATURAL_NUMBER:
                            case Y_VALUE: {
                                if (disableValue != null)
                                    obj = null;
                                break;
                            }
                            case HEALTH: {
                                obj = 20.0;
                                break;
                            }
                            case PERCENTAGE: {
                                obj = 0.0;
                                break;
                            }
                            default: {
                                throw new UnsupportedOperationException("SubType hasn't been specified for " + path);
                            }
                        }
                    }
                break;
            }
            case STRING: {
                obj = "";
                break;
            }
            case LIST: {
                obj = Collections.emptyList();
                break;
            }
            case BLOCK_RELATION_LIST: {
                obj = BlockRelationsList.EMPTY_LIST;
                break;
            }
            case POTION_EFFECT: {
                return null;
            }
            default: {
                throw new UnsupportedOperationException("Type of " + type + " doesn't have a default value to be disabled");
            }
        }
        return obj;
    }


    /**
     * Contains values for some Nodes which require a special value, which differs from other Nodes with the same type
     */
    private enum Disable {
        /**
         * A value of 0 will disable this feature in the plugin
         */
        ZERO(0),
        /**
         * A value of 1 will disable this feature in the plugin
         */
        ONE(1),
        /**
         * A value of 100 (as in percentage will disable this feature)
         */
        HUNDRED(100);

        Disable(Object obj) {
            disable = obj;
        }

        final Object disable;

        public Object get() {
            return disable;
        }
    }


    private static class DefaultWorlds extends ArrayList<String> {
        public DefaultWorlds() {
            super();
            this.add(MultiWorldConfig.ALL_WORLDS);
        }
    }
}

